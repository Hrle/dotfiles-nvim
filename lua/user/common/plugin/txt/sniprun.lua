-- NOTE: run snippets of code interactively

local M = { "michaelb/sniprun" }

-- TODO: this works in VS Code but it displays weird virtual text for some
-- TODO: reason so I'd like you to fix that
local frontend = require "user.lib.frontend"
M.disable = not (vim.fn.has "macunix" == 1 or vim.fn.has "unix" == 1)
  or (frontend.name == frontend.vscode)

M.run = "bash ./install.sh"

-- TODO: check that this is okay
M.cmd = {
  "<Plug>SnipRun",
  "<Plug>SnipRunOperator",
}

M.config = function()
  local sniprun = require "sniprun"
  local _frontend = require "user.lib.frontend"

  sniprun.setup {
    display = _frontend.has_gui and {
      "VirtualTextOk",
      "VirtualText",
    } or {
      "Classic",
    },
    live_mode_toggle = "off",
  }
end

return M
