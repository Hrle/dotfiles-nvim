-- NOTE: package manager for language servers, debuggers, formatters, linters, etc.
local M = { "williamboman/mason.nvim" }

-- NOTE: all in one file because it all requires a certain load order

-- TODO: less constraints?
-- TODO: debug check and figure out a good terminal strategy
-- TODO: debug au on cwd when launch.json exists

M.disable = not require("user.lib.frontend").has_gui

M.requires = {
  -- NOTE: language client manager
  {
    "neovim/nvim-lspconfig",
    disable = not require("user.lib.frontend").has_lsp,
    -- NOTE: they add tags for backwards compatibility
    -- tag = "*",
    requires = {
      -- NOTE: language client UI
      {
        "glepnir/lspsaga.nvim",
        disable = not require("user.lib.frontend").has_gui,
      },

      -- NOTE: language client UI for location list
      {
        "folke/trouble.nvim",
        disable = not require("user.lib.frontend").has_gui,
        requires = {
          { "kyazdani42/nvim-web-devicons" },
        },
      },

      -- NOTE: language client status UI
      {
        "j-hui/fidget.nvim",
        disable = true,
        -- disable = not require("user.lib.frontend").has_gui,
      },

      -- NOTE: generates status line components
      {
        "nvim-lua/lsp-status.nvim",
        disable = true,
      },

      -- NOTE: nice vim.lsp.buf.format wrapper
      {
        "lukas-reineke/lsp-format.nvim",
        tag = "*",
        disable = true,
      },

      -- NOTE: fuzzy search for many methods
      {
        "ojroques/nvim-lspfuzzy",
        disable = not require("user.lib.frontend").has_gui,
        requires = {
          -- NOTE: Fuzzy finder
          { "junegunn/fzf" },

          -- NOTE: Fuzzy finder preview
          { "junegunn/fzf.vim" },
        },
      },

      -- NOTE: type virtual text
      {
        "jubnzv/virtual-types.nvim",
        disable = not require("user.lib.frontend").has_gui,
      },

      -- NOTE: diagnostic virtual text
      {
        "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
        disable = not require("user.lib.frontend").has_gui,
      },

      -- NOTE: JSON schema completions
      { "b0o/schemastore.nvim" },

      -- NOTE: improved 'textDocument/definition' with omnisharp
      { "Hoffs/omnisharp-extended-lsp.nvim" },

      -- NOTE: setup Lua language client for Neovim
      { "folke/neodev.nvim" },

      -- NOTE: Bridge between mason and lspconfig
      -- Load order:
      -- 1. mason
      -- 2. mason-lspconfig
      -- 3. lspconfig
      { "williamboman/mason-lspconfig.nvim" },
    },
  },

  -- NOTE: linters, formatters, etc. (extras) manager
  {
    "jose-elias-alvarez/null-ls.nvim",
    requires = {
      -- NOTE: Async library
      { "nvim-lua/plenary.nvim" },

      -- NOTE: Bridge between mason and null-ls
      -- Load order:
      -- 1. mason
      -- 2. null-ls
      -- 3. mason-null-ls
      { "jayp0521/mason-null-ls.nvim" },
    },
  },

  -- NOTE: debugger manager
  {
    "mfussenegger/nvim-dap",
    disable = not require("user.lib.frontend").has_debug,
    tag = "*",
    requires = {
      -- NOTE: debug virtual text
      {
        "theHamsta/nvim-dap-virtual-text",
        disable = not require("user.lib.frontend").has_gui,
      },

      -- NOTE: debug UI
      {
        "rcarriga/nvim-dap-ui",
        disable = not require("user.lib.frontend").has_gui,
      },

      -- NOTE: debug adapter for Neovim Lua
      { "jbyuki/one-small-step-for-vimkind" },

      -- NOTE: Bridge between mason and dap
      -- Load order:
      -- 1. mason
      -- 2. dap
      -- 3. mason-nvim-dap
      { "jayp0521/mason-nvim-dap.nvim" },
    },
  },
}

M.config = function()
  local function server_config()
    vim.lsp.stop_client(vim.lsp.get_active_clients(), true)

    local has_neodev, neodev = pcall(require, "neodev")
    if has_neodev then
      neodev.setup {}
    end

    local has_lspsaga, lspsaga = pcall(require, "lspsaga")
    if has_lspsaga then
      lspsaga.setup {
        symbol_in_winbar = {
          enable = false,
        },
        ui = {
          border = "rounded",
          diagnostic = { "🙀", "😿", "😾", "😺" },
          code_action = "🤓",
        },
        outline = {
          win_with = "NvimTree",
          win_width = 40,
        },
      }
      -- NOTE: keeping this here just in case
      -- lspsaga.init_lsp_saga {
      --   border_style = "rounded",
      --   diagnostic_header = { "🙀", "😿", "😾", "😺" },
      --   code_action_icon = "🤓",
      --   show_outline = {
      --     win_with = "NvimTree",
      --     win_width = 40,
      --   },
      -- }
    end

    local has_trouble, trouble = pcall(require, "trouble")
    if has_trouble then
      trouble.setup {}
    end

    local has_fidget, fidget = pcall(require, "fidget")
    if has_fidget then
      fidget.setup {
        window = {
          border = "rounded",
          blend = 100,
          relative = "editor",
        },
      }
    end

    local has_lines, lines = pcall(require, "lsp_lines")
    if has_lines then
      lines.setup()
      vim.diagnostic.config {
        virtual_text = false,
      }
    end

    local has_format, format = pcall(require, "lsp-format")
    if has_format then
      format.setup {
        exclude = {
          "tsserver",
          "omnisharp",
        },
      }
    end

    local function config(pattern)
      require("user.lib.module").run_and(
        pattern,
        "(client)s?",
        function(lang_config, module_path, module_name)
          if type(lang_config) ~= "table" then
            require("user.lib.log").warn(
              "Language client configuration for language defined in module",
              module_name,
              "at",
              module_path,
              "is invalid"
            )
            return
          end

          local name = lang_config[1]
          local _config = lang_config.config
          if type(name) ~= "string" or type(_config) ~= "table" then
            require("user.lib.log").warn(
              "Language client configuration for language",
              name,
              "in module",
              module_name,
              "at",
              module_path,
              "is invalid"
            )
            return
          end

          local additional_on_attach = _config.on_attach
          _config.on_attach = function(client, buffer)
            local has_status, status = pcall(require, "lsp-status")
            if has_status then
              status.on_attach(client)
            end

            local has_virtual_types, virtual_types =
              pcall(require, "virtualtypes")
            if
              client.supports_method "textDocument/codeLens"
              and has_virtual_types
            then
              virtual_types.on_attach(client, buffer)
            end

            local _has_format, _format = pcall(require, "lsp-format")
            if _has_format then
              _format.on_attach(client)
            end

            require("user.lib.module").run(
              require("user.lib.path").conf_lua_common_user_vim_pat,
              "on_language_client_attach",
              client,
              buffer
            )

            if not require("user.lib.frontend").disabled then
              require("user.lib.module").run(
                require("user.lib.path").conf_lua_frontend_user_vim_pat,
                "on_language_client_attach",
                client,
                buffer
              )
            end

            if additional_on_attach then
              additional_on_attach(client, buffer)
            end
          end

          local additional_capabilities = _config.capabilities
          _config.capabilities = (function()
            local capabilities = vim.lsp.protocol.make_client_capabilities()
              or {}

            local has_status, status = pcall(require, "lsp-status")
            if has_status then
              capabilities =
                vim.tbl_deep_extend("force", capabilities, status.capabilities)
            end

            local has_cmp, cmp = pcall(require, "cmp_nvim_lsp")
            if has_cmp then
              capabilities = cmp.default_capabilities(capabilities)
            end

            local has_coq, coq = pcall(require, "coq")
            if has_coq then
              capabilities = coq.lsp_ensure_capabilities(capabilities)
            end

            if additional_capabilities then
              capabilities = vim.tbl_deep_extend(
                "force",
                capabilities,
                additional_capabilities
              )
            end

            return capabilities
          end)()

          require("lspconfig")[name].setup(_config)
        end
      )
    end

    local path = require "user.lib.path"
    config(path.conf_lua_common_lang_pat)
    if not require("user.lib.frontend").disabled then
      config(path.conf_lua_frontend_lang_pat)
    end
  end

  local function extra_config()
    local final_config = {
      sources = {},
    }
    local function config(pattern)
      require("user.lib.module").run_and(
        pattern,
        "(extra|linter|formatter)s?",
        function(lang_config, module_name, module_path)
          if type(lang_config) ~= "table" then
            require("user.lib.log").warn(
              "Extra configuration for language defined in module",
              module_name,
              "at",
              module_path,
              "is invalid"
            )
            return
          end

          local name = lang_config[1]
          local _config = lang_config.config
          if type(name) ~= "string" or type(_config) ~= "table" then
            require("user.lib.log").warn(
              "Extra configuration for language",
              name,
              "in module",
              module_name,
              "at",
              module_path,
              "is invalid"
            )
            return
          end

          table.insert(final_config.sources, _config)
        end
      )
    end

    local path = require "user.lib.path"
    config(path.conf_lua_common_lang_pat)
    if not require("user.lib.frontend").disabled then
      config(path.conf_lua_frontend_lang_pat)
    end

    local null_ls = require "null-ls"
    null_ls.setup(final_config)
  end

  local function debugger_config()
    local has_dap_ui, dap_ui = pcall(require, "dapui")
    if has_dap_ui then
      dap_ui.setup {}
    end

    local has_dap_vt, dap_vt = pcall(require, "nvim-dap-virtual-text")
    if has_dap_vt then
      dap_vt.setup {
        enabled = true,
        commented = true,
      }
    end

    local dap = require "dap"

    dap.defaults.fallback.terminal_win_cmd = "belowright vnew"
    dap.defaults.fallback.exception_breakpoints = "uncaught"

    -- local dap_ext_vscode = require "dap.ext.vscode"
    -- dap_ext_vscode.load_launchjs()

    dap.listeners.after.event_initialized["config"] = function()
      local _has_dap_ui, _dap_ui = pcall(require, "dapui")
      if _has_dap_ui then
        _dap_ui.open {}
      end

      local _has_tree, _tree = pcall(require, "nvim-tree.view")
      if _has_tree then
        _tree.close()
      end
    end
    dap.listeners.before.event_terminated["config"] = function()
      local _has_dap_ui, _dap_ui = pcall(require, "dapui")
      if _has_dap_ui then
        _dap_ui.close {}
      end
      dap.repl.close()
    end
    dap.listeners.before.event_exited["config"] = function()
      local _has_dap_ui, _dap_ui = pcall(require, "dapui")
      if _has_dap_ui then
        _dap_ui.close {}
      end
      dap.repl.close()
    end

    local function config(pattern)
      require("user.lib.module").run_and(
        pattern,
        "(debugger)s?",
        function(lang_config, module_path, module_name)
          if type(lang_config) ~= "table" then
            require("user.lib.log").warn(
              "Debugger configuration for language defined in module",
              module_name,
              "at",
              module_path,
              "is invalid"
            )
            return
          end

          local name = lang_config[1]
          local adapter = lang_config.adapter
          local configs = lang_config.configs
          if
            type(name) ~= "string"
            or type(adapter) ~= "table"
            or type(configs) ~= "table"
          then
            require("user.lib.log").warn(
              "Debugger configuration for",
              name,
              "in module",
              module_name,
              "at",
              module_path,
              "is invalid"
            )
            return
          end

          local _dap = require "dap"
          _dap.adapters[name] = adapter
          for filetype, _config in pairs(configs) do
            if type(filetype) ~= "string" or type(_config) ~= "table" then
              require("user.lib.log").warn(
                "Debugger configuration for",
                name,
                "of filetype",
                filetype,
                "in module",
                module_name,
                "at",
                module_path,
                "is invalid"
              )
              return
            end
            _dap.configurations[filetype] = _config
          end
        end
      )
    end

    local path = require "user.lib.path"
    config(path.conf_lua_common_lang_pat)
    if not require("user.lib.frontend").disabled then
      config(path.conf_lua_frontend_lang_pat)
    end
    dap.repl.close()
  end

  local mason = require "mason"
  mason.setup {}

  local has_mason_lspconfig, mason_lspconfig = pcall(require, "mason-lspconfig")
  if has_mason_lspconfig then
    mason_lspconfig.setup {
      automatic_installation = true,
    }
  end

  if require("user.lib.frontend").has_lsp then
    server_config()
    extra_config()
  end
  if require("user.lib.frontend").has_debug then
    debugger_config()
  end

  local has_mason_null_ls, mason_null_ls = pcall(require, "mason-null-ls")
  if has_mason_null_ls then
    mason_null_ls.setup {
      automatic_installation = true,
    }
  end

  local has_mason_dap, mason_dap = pcall(require, "mason-nvim-dap")
  if has_mason_dap then
    mason_dap.setup {
      automatic_installation = true,
    }
  end
end

return M
