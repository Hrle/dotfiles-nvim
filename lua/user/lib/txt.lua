local M = {}

function M.add_text_after_cursor(text)
  local cursor = vim.api.nvim_win_get_cursor(0)
  -- NOTE: 1 indexed
  local row = cursor[1]
  -- NOTE: 0 indexed
  local col = cursor[2]
  -- NOTE: rows and cols both 0 indexed
  vim.api.nvim_buf_set_text(0, row - 1, col, row - 1, col, text)
end

return M
