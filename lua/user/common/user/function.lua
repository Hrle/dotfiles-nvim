local M = {}

function M.preload()
  local fn = require "user.lib.fn"

  P = function(...)
    print(require("user.lib.log").concat(...))
  end
  S = fn.pack_required("user.lib.fn", "print_stack")
end

return M
