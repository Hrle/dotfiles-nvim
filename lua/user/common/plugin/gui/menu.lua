-- NOTE: Neovim menu
local M = { "nvim-telescope/telescope.nvim" }

M.disable = not require("user.lib.frontend").has_gui

local has_media_files = vim.fn.has "unix" == 1
    and vim.fn.executable "ueberzug" == 1
    and vim.fn.executable "ffmpegthumbnailer" == 1
    and vim.fn.executable "pdftoppm" == 1

M.requires = {
  -- NOTE: library
  { "nvim-lua/plenary.nvim" },

  -- NOTE: fzy sorter
  {
    "nvim-telescope/telescope-fzy-native.nvim",
    requires = {
      { "romgrk/fzy-lua-native" },
    },
  },

  -- NOTE: fzf sorter
  { "nvim-telescope/telescope-fzf-native.nvim",
    run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release; cmake --build build --config Release; cmake --install build --prefix build' },

  -- NOTE: frecency picker
  {
    "nvim-telescope/telescope-frecency.nvim",
    requires = {
      { "tami5/sqlite.lua" },
    },
  },

  -- NOTE: LSP symbol picker
  {
    "nvim-telescope/telescope-symbols.nvim",
    disable = not require("user.lib.frontend").has_lsp,
  },

  -- NOTE: media file picker
  {
    "nvim-telescope/telescope-media-files.nvim",
    disable = not has_media_files,
    requires = {
      "nvim-lua/popup.nvim",
      "nvim-lua/plenary.nvim",
    },
  },

  -- NOTE: emoji picker
  { "xiyaowong/telescope-emoji.nvim" },

  -- NOTE: session picker
  {
    "rmagatti/session-lens",
    disable = not require("user.lib.frontend").has_session,
    requires = "rmagatti/auto-session",
  },

  -- NOTE: zoxide file picker
  {
    "jvgrootveld/telescope-zoxide",
    disable = vim.fn.executable "zoxide" == 0,
  },

  -- NOTE: mapping/command picker
  { "sudormrfbin/cheatsheet.nvim" },

  -- NOTE: debug configuration picker
  {
    "nvim-telescope/telescope-dap.nvim",
    disable = not require("user.lib.frontend").has_debug,
    requires = {
      "mfussenegger/nvim-dap",
    },
  },
}

M.config = function()
  local has_lens, lens = pcall(require, "session-lens")
  if has_lens then
    lens.setup {
      path_display = { "shorten" },
      previewer = true,
    }
  end

  local has_cheatsheet, cheatsheet = pcall(require, "cheatsheet")
  if has_cheatsheet then
    local actions = require "cheatsheet.telescope.actions"
    cheatsheet.setup {
      telescope_mappings = {
        ["<CR>"] = actions.select_or_execute,
        ["<A-CR>"] = actions.select_or_fill_commandline,
      },
    }
  end

  local telescope = require "telescope"
  local actions = require "telescope.actions"
  telescope.setup {
    defaults = {
      -- NOTE: try to detect binaries and not preview them - requires `file`
      buffer_previewer_maker = function(filepath, bufnr, opts)
        local previewers = require "telescope.previewers"
        local Job = require "plenary.job"

        filepath = vim.fn.expand(filepath)
        -- TODO: not using native commands
        Job:new({
          command = "file",
          args = { "--mime-type", "-b", filepath },
          on_exit = function(j)
            local mime_type = vim.split(j:result()[1], "/", {})[1]
            if mime_type == "text" then
              previewers.buffer_previewer_maker(filepath, bufnr, opts)
            else
              vim.schedule(function()
                vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, { "BINARY" })
              end)
            end
          end,
        }):sync()
      end,
      preview = {
        timeout = 500,
      },
    },
    mappings = {
      ["<CR>"] = actions.select_vertical,
    },
    extensions = {
      fzy_native = {
        override_generic_sorter = true,
        override_file_sorter = false,
      },
      fzf = {
        fuzzy = true,
        override_generic_sorter = false,
        override_file_sorter = true,
        case_mode = "smart_case",
      },
      media_files = {
        file = {
          "png",
          "jpg",
          "jpeg",
          "gif",
          "webm",
          "webp",
          "mp4",
          "pdf",
          "epub",
        },
      },
      emoji = {
        action = function(emoji)
          local txt = require "user.lib.txt"
          txt.add_text_after_cursor { emoji.value }
        end,
      },
    },
  }

  if vim.fn.has "win32" == 1 then
    telescope.load_extension "fzf"
  else
    telescope.load_extension "fzy_native"
  end
  telescope.load_extension "frecency"
  telescope.load_extension "zoxide"
  telescope.load_extension "session-lens"
  telescope.load_extension "emoji"

  if has_media_files then
    telescope.load_extension "media_files"
  end

  local has_notify, _ = pcall(require, "notify")
  if has_notify then
    telescope.load_extension "notify"
  end

  local has_dap, _ = pcall(require, "dap")
  if has_dap then
    telescope.load_extension "dap"
  end

  local has_cmdline, _ = pcall(require, "noice")
  if has_cmdline then
    telescope.load_extension "noice"
  end

  local has_yanky, _ = pcall(require, "yanky")
  if has_yanky then
    telescope.load_extension "yank_history"
  end

  local has_project, _ = pcall(require, "project_nvim")
  if has_project then
    telescope.load_extension "projects"
  end
end

return M
