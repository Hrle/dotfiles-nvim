local M = {}

function M.ensure_dir(path)
  if vim.fn.isdirectory(path) == 0 then
    vim.fn.mkdir(path, "p")
  end
end

function M.exists(path)
  return vim.fn.filereadable(path) > 0
end

function M.exists_glob(path)
  return vim.fn.empty(vim.fn.glob(path)) <= 0
end

return M
