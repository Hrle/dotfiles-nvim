-- NOTE: file explorer for Neovim
local M = { "ms-jpq/chadtree" }

-- NOTE: disabled because it doesn't play well with my theme
-- M.disable = not require("user.lib.frontend").has_gui
M.disable = true

M.branch = "chad"

M.run = "python3 -m chadtree deps"

M.requires = {
  -- NOTE: icon mappings for NERD fonts
  { "nvim-tree/nvim-web-devicons" },
}

M.setup = function()
  vim.g.chadtree_settings = {}
end

return M
