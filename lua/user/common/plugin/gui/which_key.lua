-- NOTE: reminds you of mappings with a floating window
local M = { "folke/which-key.nvim" }

M.disable = not require("user.lib.frontend").has_gui

M.config = function()
  local which_key = require "which-key"

  which_key.setup {
    marks = true,
    registers = true,
    window = {
      border = "single",
    },
  }

  vim.cmd [[ hi! link WhichKeyFloat Normal ]]
  vim.cmd [[ hi! link WhichKeyBorder Float ]]
end

return M
