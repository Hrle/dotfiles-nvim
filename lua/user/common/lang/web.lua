local M = {}

function M.html_client()
  return {
    "html",
    config = {
      capabilities = {
        textDocument = {
          completion = {
            completionItem = {
              snippetSupport = true,
            },
          },
        },
      },
    },
  }
end

function M.json_client()
  return {
    "jsonls",
    config = {
      settings = {
        json = {
          schemas = require("schemastore").json.schemas(),
        },
      },
    },
  }
end

function M.prettier_formatter()
  return {
    "prettier",
    config = require("null-ls").builtins.formatting.prettier,
  }
end

function M.tsserver_client()
  return {
    "tsserver",
    config = {
      -- Ignores File is a CommonJS module; it may be converted to an ES module.
      -- Shamelessly stolen from:
      -- https://www.reddit.com/r/neovim/comments/nv3qh8/how_to_ignore_tsserver_error_file_is_a_commonjs/
      handlers = {
        ["textDocument/publishDiagnostics"] = function(
          err,
          result,
          ctx,
          config
        )
          if
            result ~= nil
            and result.diagnostics ~= nil
            and type(result.diagnostics) == "table"
          then
            local idx = 1
            while idx <= #result.diagnostics do
              if result.diagnostics[idx].code == 80001 then
                table.remove(result.diagnostics, idx)
              else
                idx = idx + 1
              end
            end
          end
          vim.lsp.diagnostic.on_publish_diagnostics(err, result, ctx, config)
        end,
      },
    },
  }
end

-- TODO: figure this out?
-- function M.eslint_client()
--   local eslint_setup = {
--     cmd = { "vscode-eslint-language-server", "--stdio" },
--     root_dir = function(file, bufnr)
--       local util = require "lspconfig.util"
--
--       local clients = vim.lsp.buf_get_clients(bufnr)
--       for _, client in ipairs(clients) do
--         if client.name == "tsserver" then
--           return nil
--         end
--       end
--       P(vim.tbl_map(function(client)
--         return client.name
--       end, clients))
--
--       return util.find_package_json_ancestor(file)
--     end,
--   }
--
--   lspconfig["eslint"].setup(tbl.merge(setup, eslint_setup))
-- end

function M.svelte_client()
  return {
    "svelte",
    config = {},
  }
end

function M.cssls_client()
  local capabilities = vim.lsp.protocol.make_client_capabilities()
  capabilities.textDocument.completion.completionItem.snippetSupport = true
  return {
    "cssls",
    config = {
      capabilities = capabilities,
      filetypes = {
        "css",
        "scss",
        "less",
        "javascriptreact",
        "typescriptreact",
        "javascript",
        "typescript",
      },
    },
  }
end

-- NOTE: this has 14 stars on github...
-- function M.stylelint_client()
--   return {
--     "stylelint_lsp",
--     config = {
--       settings = {
--         stylelintplus = {
--           -- see available options in stylelint-lsp documentation
--         },
--       },
--     },
--   }
-- end

M.ecma_injections = [[
(comment) @jsdoc
(comment) @comment
(regex_pattern) @regex

; =============================================================================
; languages

; {lang}`<{lang}>`
(call_expression
function: ((identifier) @language)
arguments: ((template_string) @content
  (#offset! @content 0 1 0 -1)
  (#inject_without_children! @content)))

; gql`<graphql>`
(call_expression
function: ((identifier) @_name
  (#eq? @_name "gql"))
arguments: ((template_string) @graphql
  (#offset! @graphql 0 1 0 -1)
  (#inject_without_children! @graphql)))

; hbs`<glimmer>`
(call_expression
function: ((identifier) @_name
  (#eq? @_name "hbs"))
arguments: ((template_string) @glimmer
  (#offset! @glimmer 0 1 0 -1)
  (#inject_without_children! @glimmer)))

; =============================================================================
; styled-components

; styled.div`<css>`
(call_expression
function: (member_expression
  object: (identifier) @_name
    (#eq? @_name "styled"))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; styled(Component)`<css>`
(call_expression
function: (call_expression
  function: (identifier) @_name
    (#eq? @_name "styled"))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; styled.div.attrs({ prop: "foo" })`<css>`
(call_expression
function: (call_expression
  function: (member_expression
    object: (member_expression
      object: (identifier) @_name
        (#eq? @_name "styled"))))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; styled(Component).attrs({ prop: "foo" })`<css>`
(call_expression
function: (call_expression
  function: (member_expression
    object: (call_expression
      function: (identifier) @_name
        (#eq? @_name "styled"))))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; createGlobalStyle`<css>`
(call_expression
function: (identifier) @_name
  (#eq? @_name "createGlobalStyle")
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))
]]

function M.styled_components_syntax()
  -- NOTE: no JSX parser
  vim.treesitter.query.set_query("javascript", "injections", M.ecma_injections)
  vim.treesitter.query.set_query("typescript", "injections", M.ecma_injections)
  vim.treesitter.query.set_query("tsx", "injections", M.ecma_injections)
end

return M
