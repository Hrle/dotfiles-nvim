-- NOTE: allows you to use sudo nicely in Neovim

local M = { "lambdalisue/suda.vim" }

M.cmd = { "SudaRead", "SudaWrite" }

return M
