-- NOTE: manage sqlite databases when ShaDa is not enough

local M = { "kkharji/sqlite.lua" }

M.config = function()
  if vim.fn.has "win32" == 1 then
    vim.g.sqlite_clib_path = require("user.lib.path").sqlite_dll_loc
  end
end

return M
