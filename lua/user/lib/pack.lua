local M = {}

function M.is_bootstrapped()
  local path = require "user.lib.path"
  local fs = require "user.lib.fs"

  return fs.exists_glob(path.data_packer_install_dir)
end

function M.bootstrap()
  local path = require "user.lib.path"
  local fs = require "user.lib.fs"

  if fs.exists_glob(path.data_packer_install_dir) then
    return true
  end

  local did_bootstrap = vim.fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    path.data_packer_install_dir,
  }

  if not did_bootstrap then
    require("user.lib.log").error "Failed bootstrapping"
    return false
  end

  require("user.lib.log").info "Bootstrapped"
  return true
end

local function init_unsafe()
  vim.cmd [[packadd packer.nvim]]

  local packer = require "packer"
  local path = require "user.lib.path"
  local frontend = require "user.lib.frontend"

  packer.init {
    package_root = path.data_pack_dir,
    compile_path = path.conf_lua_packer_compiled_loc,
    snapshot_path = path.cache_packer_snapshot_dir,
    max_jobs = 20,
    git = { clone_timeout = 1000 },
    profile = { enable = true, threshold = 1 },
    display = frontend.has_gui
        and {
          open_fn = function()
            return require("packer.util").float {
              border = "rounded",
              -- TODO: transparency
              -- win_options = {
              --   winhighlight = {
              --     Normal = "Normal",
              --     FloatBorder = "Float"
              --   }
              -- }
            }
          end,
        }
      or {
        non_interactive = true,
      },
  }

  packer.reset()

  return true
end

function M.init()
  local fn = require "user.lib.fn"

  local initialized = fn.try(init_unsafe, "Failed initializing packer")
  if not initialized then
    return false
  end

  require("user.lib.log").info "Initialized packer"
  return true
end

local function declare_plugin_spec_unsafe(spec, path, name)
  local packer = require "packer"

  if type(spec.config) ~= "table" then
    if not spec.config then
      spec.config = {}
    else
      spec.config = { spec.config }
    end
  end
  local module = require "user.lib.module"
  local escape = require("user.lib.path").escape
  local plugin = spec[1]
  local function_suffix = module.tail(path)
  local vim_module_path = module.mirror_vim_module_path(path)

  -- NOTE: injection like this because they use string.dump which expects a
  -- function with no captures (no metatable shenanigans either - __call is used
  -- on tables and string.dump requires a function)

  table.insert(
    spec.config,
    string.format(
      [[
        require("user.lib.module").run(
          require("user.lib.path").conf_lua_common_vim_pat,
          "%s$"
        )
      ]],
      function_suffix
    )
  )

  if not require("user.lib.frontend").disabled then
    table.insert(
      spec.config,
      string.format(
        [[
          require("user.lib.module").run(
            require("user.lib.path").conf_lua_frontend_vim_pat,
            "%s$"
          )
        ]],
        function_suffix
      )
    )
  end

  table.insert(
    spec.config,
    string.format(
      [[
        if require("user.lib.fs").exists "%s" then
          require("user.lib.module").source "%s"
        end
      ]],
      escape(vim_module_path),
      escape(vim_module_path)
    )
  )
  table.insert(
    spec.config,
    string.format(
      [[
        require("user.lib.log").info("Loaded", "%s", "as", "%s", "at", "%s")
      ]],
      plugin,
      escape(name),
      escape(vim_module_path)
    )
  )

  packer.use(spec)
  require("user.lib.log").info("Declared", plugin, "as", name, "at", path)

  return true
end

local function declare_rock_spec_unsafe(spec, path, name)
  local packer = require "packer"
  local plugin = spec[1]

  packer.use_rocks(spec)
  require("user.lib.log").info("Declared", plugin, "as", name, "at", path)

  return true
end

local function declare_plugin_unsafe(path)
  local module = require "user.lib.module"

  module.foreach(path, declare_plugin_spec_unsafe)

  return true
end

local function declare_rock_unsafe(path)
  local module = require "user.lib.module"

  module.foreach(path, declare_rock_spec_unsafe)

  return true
end

function M.declare_plugin(path)
  local fn = require "user.lib.fn"

  local did_declare_plugin = fn.try(
    declare_plugin_unsafe,
    string.format("Failed declaring plugin at %s", path),
    path
  )

  if not did_declare_plugin then
    return false
  end

  return true
end

function M.declare_rock(path)
  local fn = require "user.lib.fn"

  local did_declare_rock = fn.try(
    declare_rock_unsafe,
    string.format("Failed declaring rock at %s", path),
    path
  )

  if not did_declare_rock then
    return false
  end

  return true
end

function M.declare()
  local fn = require "user.lib.fn"
  local path = require "user.lib.path"
  local packer = require "packer"

  packer.reset()

  local declared_common_plugins = fn.try(
    declare_plugin_unsafe,
    "Failed declaring common plugins",
    path.conf_lua_common_plugin_pat
  )
  if not declared_common_plugins then
    return false
  end
  local declared_common_rocks = fn.try(
    declare_rock_unsafe,
    "Failed declaring common rocks",
    path.conf_lua_common_rocks_pat
  )
  if not declared_common_rocks then
    return false
  end
  require("user.lib.log").info "Declared common plugins"
  if require("user.lib.frontend").disabled then
    return true
  end

  local declared_frontend_plugins = fn.try(
    declare_plugin_unsafe,
    "Failed declaring frontend plugins",
    path.conf_lua_frontend_plugin_pat
  )
  if not declared_frontend_plugins then
    return false
  end
  local declared_frontend_rocks = fn.try(
    declare_rock_unsafe,
    "Failed declaring frontend rocks",
    path.conf_lua_frontend_rocks_pat
  )
  if not declared_frontend_rocks then
    return false
  end
  require("user.lib.log").info "Declared frontend plugins"
  return true
end

function M.sync()
  local packer = require "packer"
  local pack = require "user.lib.pack"

  if not pack.declare() then
    return false
  end

  require("user.lib.log").info "Syncing plugins"
  packer.sync()

  -- TODO: wait synchronously like for compile
  require("user.lib.log").warn(
    "Please reload Neovim when syncing is over",
    "because there is more setup to be done afterwards and",
    "I can't wait for syncing because it is async"
  )

  return true
end

function M.compile()
  local packer = require "packer"
  local fs = require "user.lib.fs"
  local path = require "user.lib.path"
  local pack = require "user.lib.pack"
  local log = require "user.lib.log"

  if not pack.declare() then
    return false
  end

  log.debug "Compiling plugins"
  vim.lsp.stop_client(vim.lsp.get_active_clients())
  packer.compile()

  -- NOTE: compile is async
  vim.wait(5000, function()
    local local_fs = require "user.lib.fs"
    local local_path = require "user.lib.path"
    return local_fs.exists(local_path.conf_lua_packer_compiled_loc)
  end)
  if not fs.exists(path.conf_lua_packer_compiled_loc) then
    log.error "Failed compiling plugins"
    return false
  end

  log.info "Compiled plugins"
  return true
end

-- TODO: clean up compile calls a bit
function M.load()
  local fn = require "user.lib.fn"
  local fs = require "user.lib.fs"
  local path = require "user.lib.path"
  local pack = require "user.lib.pack"

  if not fs.exists(path.conf_lua_packer_compiled_loc) then
    if not pack.compile() then
      return false
    end

    require("user.lib.log").info "Loaded and compiled plugins"
    return true
  end

  if
    not fn.try(function(loc)
      dofile(loc)
      return true
    end, "Failed loading plugins", path.conf_lua_packer_compiled_loc)
  then
    return false
  end

  -- NOTE: this is a test that the already compiled config is working properly
  local has_plenary, _ = pcall(require, "plenary")
  if not has_plenary then
    if not pack.compile() then
      return false
    end

    require("user.lib.log").info "Loaded and compiled plugins"
    return true
  end

  require("user.lib.log").info "Loaded plugins"
  return true
end

return M
