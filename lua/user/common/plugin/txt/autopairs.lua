-- NOTE: automatically add pairs of brackets

local M = { "windwp/nvim-autopairs" }

M.config = function()
  local autopairs = require "nvim-autopairs"

  local has_treesitter, _ = pcall(require, "treesitter")
  local check_ts = false
  local enable_check_bracket_line = true
  if has_treesitter then
    check_ts = true
    enable_check_bracket_line = false
  end

  autopairs.setup {
    disable_filetype = { "TelescopePrompt", "NvimTree" },
    check_ts = check_ts,
    enable_check_bracket_line = enable_check_bracket_line,
  }
end

return M
