-- NOTE: fuzzy finder for Neovim
local M = { "junegunn/fzf.vim" }

M.disable = not require("user.lib.frontend").has_gui

M.requires = {
  -- NOTE: written in Go
  {
    "junegunn/fzf",
    run = ":call fzf#install()",
  },
  -- NOTE: written in C
  {
    "nvim-telescope/telescope-fzf-native.nvim",
    run = "make",
  },

  -- NOTE: bindings for existing fzf
  {
    "ibhagwan/fzf-lua",
    disable = not (vim.fn.executable "fzf" == 1),
  },
}

return M
