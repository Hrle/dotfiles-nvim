local M = {}

function M.solargraph_client()
  return {
    "solargraph",
    config = {}
  }
end

function M.rubucop_formatter()
  return {
    "rubocop",
    config = require("null-ls").builtins.formatting.rubocop,
  }
end

return M
