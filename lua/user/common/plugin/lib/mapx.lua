-- NOTE: makes mappings easier and has out of the box which-key integration

local M = { "b0o/mapx.nvim" }

if require("user.lib.frontend").has_gui then
  M.requires = { "folke/which-key.nvim" }
  M.after = "which-key.nvim"
end

M.module = "mapx"

M.config = function()
  local mapx = require "mapx"

  mapx.setup {
    whichkey = require("user.lib.frontend").has_gui,
  }
end

return M
