-- NOTE: Git client inside Neovim
local M = { "TimUntersberger/neogit" }

-- TODO: mapping

M.disable = not require("user.lib.frontend").has_gui

M.requires = {
  -- NOTE: library
  { "nvim-lua/plenary.nvim" },

  -- NOTE: renders git signs in buffers and enables simple git actions
  { "lewis6991/gitsigns.nvim" },
}

M.config = function()
  local gitsigns = require "gitsigns"
  gitsigns.setup {
    keymaps = nil,
    on_attach = function(buffer)
      local module = require "user.lib.module"
      local path = require "user.lib.path"
      module.run(path.conf_lua_common_vim_pat, "git", buffer)
      module.run(path.conf_lua_frontend_vim_pat, "git", buffer)
    end,
  }

  local neogit = require "neogit"
  neogit.setup {}
end

return M
