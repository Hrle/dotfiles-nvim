local M = {}

-- TODO: gui/not gui

function M.postload()
  local hydra = require "hydra"
  local frontend = require "user.lib.frontend"
  local user = require "user.lib.user"

  if frontend.has_gui then
    hydra {
      name = "Draw Diagram",
      hint = [[
Arrow^^^^^^   Select region with <C-v>
^ ^ _K_ ^ ^   _f_: surround it with box
_H_ ^ ^ _L_
^ ^ _J_ ^ ^                      _<Esc>_
]],
      config = {
        color = "pink",
        invoke_on_body = true,
        hint = {
          border = "rounded",
        },
        on_enter = function()
          vim.o.virtualedit = "all"
        end,
      },
      mode = "n",
      body = "<leader>d",
      heads = {
        { "H", "<C-v>h:VBox<CR>" },
        { "J", "<C-v>j:VBox<CR>" },
        { "K", "<C-v>k:VBox<CR>" },
        { "L", "<C-v>l:VBox<CR>" },
        { "f", ":VBox<CR>", { mode = "v" } },
        { "<Esc>", nil, { exit = true } },
      },
    }
  end

  local yanky_hydra = hydra {
    name = "Yank ring",
    hint = [[
_<C-n>_ - cycle forward
_<C-p>_ - cycle backward

_<Esc>_ - exit
]],
    config = {
      hint = {
        border = "rounded",
      },
    },
    mode = "n",
    heads = {
      { "<C-n>", "<Plug>(YankyCycleForward)" },
      { "<C-p>", "<Plug>(YankyCycleBackward)" },
      { "<Esc>", nil, { exit = true } },
    },
  }

  local function make_activate_yanky_hydra(action)
    return function()
      vim.fn.feedkeys(vim.api.nvim_replace_termcodes(action, true, true, true))
      yanky_hydra:activate()
    end
  end

  user.keymaps {
    {
      mode = { "n", "x" },
      "p",
      make_activate_yanky_hydra "<Plug>(YankyPutAfter)",
    },
    {
      mode = { "n", "x" },
      "P",
      make_activate_yanky_hydra "<Plug>(YankyPutBefore)",
    },
    {
      mode = { "n", "x" },
      "gp",
      make_activate_yanky_hydra "<Plug>(YankyGPutAfter)",
    },
    {
      mode = { "n", "x" },
      "gP",
      make_activate_yanky_hydra "<Plug>(YankyGPutBefore)",
    },
  }
end

return M
