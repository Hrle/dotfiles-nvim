-- NOTE: neovim submodes

local M = { "anuvyklack/hydra.nvim" }

M.after = "legendary.nvim"

M.module = "hydra"

return M
