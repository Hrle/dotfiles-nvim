-- NOTE: fuzzy finder for Neovim
local M = { "nvim-telescope/telescope-fzy-native.nvim" }

M.disable = not require("user.lib.frontend").has_gui

M.requires = {
  -- NOTE: written in C
  {
    "romgrk/fzy-lua-native",
    run = "make",
  },
}

return M
