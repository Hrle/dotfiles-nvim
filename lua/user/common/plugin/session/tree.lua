-- NOTE: big daddy of Neovim directory tree

local M = { "kyazdani42/nvim-tree.lua" }

-- TODO: learn more mappings

M.requires = {
  -- NOTE: icnos
  { "kyazdani42/nvim-web-devicons" },
}

local frontend = require "user.lib.frontend"
M.disable = not (frontend.has_session and frontend.has_gui)

M.config = function()
  local special_md_txt_files = {
    "README",
    "CHANGELOG",
    "LICENSE",
    "SUPPORT",
    "SECURITY",
    "CODE_OF_CONDUCT",
    "CONTRIBUTING",
    "CONTRIBUTORS",
    "AUTHORS",
    "ACKNOWLEDGMENTS",
    "CODEOWNERS",
    "ISSUE_TEMPLATE",
    "PULL_REQUEST_TEMPLATE",
  }
  local special_files = {}
  for _, special_file in pairs(special_md_txt_files) do
    special_files[special_file] = 1
    special_files[special_file .. ".txt"] = 1
    special_files[special_file .. ".md"] = 1
  end

  local tree = require "nvim-tree"
  tree.setup {
    notify = {
      threshold = vim.log.levels.ERROR,
    },
    hijack_cursor = true,
    respect_buf_cwd = false,
    sync_root_with_cwd = true,
    update_focused_file = {
      enable = true,
    },
    diagnostics = { enable = true },
    renderer = {
      add_trailing = true,
      highlight_git = true,
      highlight_opened_files = "icon",
      group_empty = false,
      special_files = special_files,
      icons = {
        show = {
          git = true,
          folder = true,
          file = true,
          folder_arrow = true,
        },
      },
    },
    view = {
      width = 40,
      hide_root_folder = true,
      signcolumn = "no",
      -- NOTE: this is cool but it drives me insane sometimes
      -- adaptive_size = true,
      side = "right",
      mappings = {
        list = {
          { key = "d", action = "trash" },
          { key = "D", action = "remove" },
        },
      },
    },
    git = {
      enable = true,
      ignore = false,
      timeout = 500,
    },
    filters = {
      custom = {
        "^\\.cache",
        "^\\.git",
        "^node_modules",
      },
      exclude = {
        ".gitignore",
        ".gitconfig",
        ".gitexcludes",
        ".gitmodules",
        ".git/config",
      },
    },
  }
end

return M
