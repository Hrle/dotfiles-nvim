local M = { "ahmedkhalf/project.nvim" }

M.disable = true
-- M.disable = not require("user.lib.frontend").has_session

function M.config()
  local project = require "project_nvim"
  project.setup {
    patterns = {
      ".git",
      "_darcs",
      ".hg",
      ".bzr",
      ".svn",
      "Makefile",
      "package.json",
      ".luarc.json",
    },
  }
end

return M
