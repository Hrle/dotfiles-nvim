local M = {}

function M.sqlfluff_linter()
  return {
    "sqlfluff",
    config = require("null-ls").builtins.diagnostics.sqlfluff.with {
      extra_args = { "--dialect", "postgres" },
    },
  }
end

function M.sql_formatter()
  return {
    "sql_formattter",
    config = require("null-ls").builtins.formatting.sql_formatter.with {
      extra_args = { "-l", "postgresql" },
    },
  }
end

return M
