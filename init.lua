local M = {}

function M.init()
  local path = require "user.lib.path"
  if not path.setup() then
    return false
  end

  require "user.lib.cache"
  -- TODO: some kind of handling?
  -- local cache = require "user.lib.cache"
  -- if not cache.setup() then
  -- end

  local user = require "user.lib.user"
  -- NOTE: enables some basic options
  if not user.preload() then
    return false
  end

  local pack = require "user.lib.pack"
  if not pack.is_bootstrapped() then
    if not pack.bootstrap() then
      return false
    end
    if not pack.init() then
      return false
    end
    if not pack.sync() then
      return false
    end
    -- NOTE: no user.setup here because it might require some plugins that are
    -- not synced because pack.sync is not synchronous
    return true
  else
    if not pack.init() then
      return false
    end
    if not pack.load() then
      return false
    end
  end

  if not user.postload() then
    return false
  end

  return true
end

M.init()

return M
