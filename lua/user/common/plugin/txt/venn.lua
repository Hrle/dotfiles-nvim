-- NOTE: draw ASCII Venn diagrams

local M = { "jbyuki/venn.nvim" }

M.cmd = { "VBox" }

M.disable = not require("user.lib.frontend").has_gui

return M
