-- NOTE: Neovim syntax
local M = { "nvim-treesitter/nvim-treesitter" }

-- TODO: mappings
-- TODO: textobjects
-- TODO: foldexpr in lua

M.disable = not require("user.lib.frontend").has_treesitter

M.requires = {
  -- NOTE: add rainbow colors to pairs
  { "p00f/nvim-ts-rainbow" },

  -- NOTE: automatically add/delete/cut HTML/XML/... tags
  { "windwp/nvim-ts-autotag" },

  -- NOTE: sets the commentstring depending on the cursor position
  { "JoosepAlviste/nvim-ts-context-commentstring" },

  -- NOTE: adds treesitter powered text objects
  { "nvim-treesitter/nvim-treesitter-textobjects" },

  -- NOTE: adds nice virtual text at the end of the current scope
  { "haringsrob/nvim_context_vt" },

  -- NOTE: makes developing queries easier
  { "nvim-treesitter/playground" },

  -- NOTE: comment selection, range, a line
  { "numToStr/Comment.nvim" },
}

M.run = ":TSUpdate"

function M.config()
  local treesitter_config = require "nvim-treesitter.configs"
  treesitter_config.setup {
    ensure_installed = "all",
    query_linter = {
      enable = true,
      use_virtual_text = true,
      lint_events = { "BufWrite", "CursorHold" },
    },
    playground = {
      enable = true,
    },
    highlight = {
      enable = true,
    },
    indent = {
      enable = true,
    },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "gnn",
        node_incremental = "grn",
        scope_incremental = "grc",
        node_decremental = "grm",
      },
    },
    rainbow = {
      enable = true,
      extended_mode = true,
    },
    autotag = {
      enable = true,
    },
    context_commentstring = {
      enable = true,
      enable_autocmd = false,
    },
    textobjects = {
      select = {
        enable = true,
        lookahead = true,
      },
      swap = {
        enable = true,
      },
      move = {
        enable = true,
        set_jumps = true,
      },
    },
  }

  local comment = require "Comment"
  comment.setup {
    pre_hook = require("ts_context_commentstring.integrations.comment_nvim").create_pre_hook(),
  }

  local module = require "user.lib.module"
  local path = require "user.lib.path"
  module.run(path.conf_lua_common_lang_pat, "syntax")
  module.run(path.conf_lua_frontend_lang_pat, "syntax")
end

return M
