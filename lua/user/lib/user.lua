local M = {}

local function preload_unsafe()
  local module = require "user.lib.module"
  local path = require "user.lib.path"

  module.source_glob(path.conf_vim_common_user_pat)
  module.run(path.conf_lua_common_user_pat, "preload$")
  require("user.lib.log").info "Preloaded common user settings"
  if require("user.lib.frontend").disabled then
    return true
  end

  module.source_glob(path.conf_vim_frontend_user_pat)
  module.run(path.conf_lua_frontend_user_pat, "preload$")
  require("user.lib.log").info "Preloaded frontend user settings"

  return true
end

local function postload_unsafe()
  local module = require "user.lib.module"
  local path = require "user.lib.path"

  module.source_glob(path.conf_vim_common_user_pat)
  module.run(path.conf_lua_common_user_pat, "postload$")
  require("user.lib.log").info "Postloaded common user settings"
  if require("user.lib.frontend").disabled then
    return true
  end

  module.source_glob(path.conf_vim_frontend_user_pat)
  module.run(path.conf_lua_frontend_user_pat, "postload$")
  require("user.lib.log").info "Postloaded frontend user settings"

  return true
end

function M.preload()
  local fn = require "user.lib.fn"

  return fn.try(preload_unsafe, "Failed running user preload config")
end

function M.postload()
  local fn = require "user.lib.fn"

  return fn.try(postload_unsafe, "Failed running user postload config")
end

function M.keymaps(keymaps)
  local legendary = require "legendary"

  -- NOTE: inject noremap if not specified
  for _, keymap in ipairs(keymaps) do
    if not keymap.opts then
      keymap.opts = {}
    end

    if type(keymap.opts.noremap) ~= "boolean" then
      keymap.opts.noremap = true
    end
  end

  legendary.keymaps(keymaps)
end

function M.commands(commands)
  local legendary = require "legendary"

  legendary.commands(commands)
end

function M.autocmds(autocmds)
  local legendary = require "legendary"

  -- NOTE: inject clear if not specified
  for _, keymap in ipairs(autocmds) do
    if type(keymap.clear) ~= "boolean" then
      keymap.clear = true
    end
  end

  legendary.autocmds(autocmds)
end

function M.functions(functions)
  local legendary = require "legendary"

  legendary.funcs(functions)
end

return M
