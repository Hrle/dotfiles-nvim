local M = {}

function M.lua_client()
  return {
    "sumneko_lua",
    config = {
      single_file_support = false,
    },
  }
end

function M.vimls_client()
  return {
    "vimls",
    config = {},
  }
end

function M.stylua_formatter()
  return {
    "stylua",
    config = require("null-ls").builtins.formatting.stylua,
  }
end

return M
