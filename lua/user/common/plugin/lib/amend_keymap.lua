-- NOTE: allows amending the keymap in Neovim
local M = { "anuvyklack/keymap-amend.nvim" }

return M
