-- NOTE: better yanking support

local M = { "gbprod/yanky.nvim" }

M.requires = vim.fn.has "win32" == 0 and { "kkharji/sqlite.lua" } or nil

M.config = function()
  local yanky = require "yanky"

  yanky.setup {
    ring = {
      history_length = 1000,
      storage = vim.fn.has "win32" == 0 and "sqlite" or nil,
    },
    highlight = {
      on_put = true,
      on_yank = true,
      timer = 500,
    },
  }
end

return M
