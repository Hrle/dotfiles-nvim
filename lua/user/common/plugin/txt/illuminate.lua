-- NOTE: highlights word under cursor

local M = { "RRethy/vim-illuminate" }

local frontend = require "user.lib.frontend"
M.disable = not (frontend.has_gui or frontend.name == frontend.vscode)

M.config = function()
  local illuminate = require "illuminate"
  local _frontend = require "user.lib.frontend"

  illuminate.configure {
    providers = _frontend.name == _frontend.vscode and {
      "regex",
    } or {
      "lsp",
      "treesitter",
      "regex",
    },
    filetypes_denylist = {
      "NvimTree",
      "Startup",
      "Telescope",
    },
  }
end

return M
