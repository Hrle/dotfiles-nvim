local M = {}

-- NOTE: this works with userdata while Neovim functions don't

function M.merge(...)
  local result = {}

  for _, tbl in ipairs { ... } do
    for key, value in pairs(tbl) do
      result[key] = value
    end
  end

  return result
end

function M.copy(tbl)
  return M.merge(tbl)
end

function M.dmerge(...)
  local result = M.merge(...)

  for key, value in pairs(result) do
    if type(value) == "table" then
      local key_result = {}

      for _, current in ipairs { ... } do
        if type(current[key]) == "table" then
          table.insert(key_result, current[key])
        end
      end

      result[key] = M.dmerge(unpack(key_result))
    end
  end

  return result
end

function M.dcopy(tbl)
  return M.dmerge(tbl)
end

function M.contains(tbl, value)
  for _, current in pairs(tbl) do
    if current == value then
      return true
    end
  end

  return false
end

return M
