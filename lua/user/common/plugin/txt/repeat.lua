-- NOTE: enables command repetition for various plugins

local M = { "tpope/vim-repeat" }

return M
