local M = { "MunifTanjim/nui.nvim" }

M.disable = not require("user.lib.frontend").has_gui

return M
