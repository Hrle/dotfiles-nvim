local M = { "max397574/better-escape.nvim" }

-- TODO: make this work in VS Code?
local frontend = require "user.lib.frontend"
M.disable = frontend.name == frontend.vscode

M.config = function()
  local escape = require "better_escape"

  escape.setup {
    timeout = 500,
  }
end

return M
