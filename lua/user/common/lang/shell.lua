local M = {}

function M.bashls_client()
  return {
    "bashls",
    config = {},
  }
end

function M.shfmt_formatter()
  return {
    "prettier",
    config = require("null-ls").builtins.formatting.shfmt.with {
      extra_args = { "-i", "2", "-ci" },
    },
  }
end

return M
