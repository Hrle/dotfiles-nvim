local M = {}

-- TODO: gui/not gui

function M.postload()
  local user = require "user.lib.user"

  user.autocmds {
    {
      name = "UserLspSaga",
      {
        "Filetype",
        function()
          vim.wo.foldmethod = "manual"
        end,
        opts = {
          pattern = "lspsagaoutline",
        },
      },
    },
    {
      name = "UserRust",
      {
        "Filetype",
        function()
          -- TODO: rustfmt settings?
          -- vim.bo.tabstop = 2
          -- vim.bo.shiftwidth = 2
        end,
        opts = {
          pattern = "rust",
        },
      },
    },
    {
      name = "UserTrouble",
      {
        "BufEnter",
        function()
          local trouble = require "trouble"
          trouble.refresh {
            auto = true,
          }
        end,
        opts = {
          pattern = "Trouble",
        },
      },
    },
  }
end

function M.on_language_client_attach()
  local user = require "user.lib.user"

  user.autocmds {
    {
      name = "UserLsp",
      {
        "BufWritePre",
        function()
          vim.lsp.buf.format {
            filter = function(client)
              return client.name ~= "tsserver" and client.name ~= "omnisharp"
            end,
          }
        end,
      },
    },
  }
end

return M
