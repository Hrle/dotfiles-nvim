local M = {}

function M.find_executable(dir)
  local debug_path = dir
  local debug_contents = vim.fn.readdir(debug_path)
  local program_path = nil
  for _, file in ipairs(debug_contents) do
    local file_path = debug_path .. "/" .. file
    if
      vim.fn.executable(file_path) == 1
      and vim.fn.isdirectory(file_path) == 0
    then
      program_path = file_path
    end
  end
  if not program_path then
    return vim.fn.input {
      prompt = "Path to executable:",
      vim.fn.getcwd() .. "/",
      "executable",
    }
  end

  return program_path
end

-- TODO: cross-platform method
function M.pgrep(name)
  local pgrep_result = vim.fn.system("pgrep -f '" .. name .. "'")
  local pid = tonumber(pgrep_result)
  return pid
end

-- TODO: cross-platform method
function M.which(name)
  local which_result = vim.fn.system("which '" .. name .. "'")
  -- NOTE: which returns a newline at the end
  local lldb_path = string.sub(which_result, 1, -2)
  return lldb_path
end

return M
