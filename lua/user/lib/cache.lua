local M = {}

function M.setup()
  local path = require "user.lib.path"
  _G.__luacache_config = {
    chunks = {
      enable = true,
      path = path.cache_frontend_dir .. "/luacache_chunks",
    },
    modpaths = {
      enable = true,
      path = path.cache_frontend_dir .. "/luacache_modpaths",
    },
  }

  local setup_cache, _ = pcall(require, "impatient")
  if not setup_cache then
    require("user.lib.log").warn "Failed setting up cache"
    return false
  end

  return true
end

return M
