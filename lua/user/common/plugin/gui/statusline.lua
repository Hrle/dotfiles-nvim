-- NOTE: a statusline

local M = { "hoob3rt/lualine.nvim" }

M.disable = not require("user.lib.frontend").has_gui

M.requires = {
  -- NOTE: devicons
  { "kyazdani42/nvim-web-devicons" },
}

M.config = function()
  local lualine = require "lualine"

  local has_hydra, _ = pcall(require, "hydra.statusline")
  local has_treesitter, _ = pcall(require, "nvim-treesitter")
  local has_edge, edge = pcall(require, "lualine.themes.edge")

  lualine.setup {
    options = {
      theme = has_edge and edge or nil,
      section_separators = { left = "", right = "" },
      component_separators = { left = "", right = "" },
    },
    sections = {
      lualine_a = { "mode" },
      lualine_b = {
        has_hydra and {
          function()
            local hydra = require "hydra.statusline"
            return string.format("%s %s", hydra.get_color(), hydra.get_name())
          end,
          cond = function()
            local hydra = require "hydra.statusline"
            return hydra.is_active()
          end,
        } or nil,
        (has_treesitter and false) and function()
          local treesitter = require "nvim-treesitter"
          return string.format(
            "%s",
            treesitter.statusline { indicator_size = 30 }
          )
        end or nil,
      },
      lualine_c = {},
      lualine_x = {},
      lualine_y = { "filename", "filetype" },
      lualine_z = { "branch" },
    },
    inactive_sections = {
      lualine_a = {},
      lualine_b = {},
      lualine_c = { "filename" },
      lualine_x = { "location" },
      lualine_y = {},
      lualine_z = {},
    },
  }
end

return M
