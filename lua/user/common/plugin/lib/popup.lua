-- NOTE: popup API for Neovim that will probably get upstreamed
local M = { "nvim-lua/popup.nvim" }

M.disable = not require("user.lib.frontend").has_gui

return M
