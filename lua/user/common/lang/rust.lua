local M = {}

function M.rust_analyzer_client()
  return {
    "rust_analyzer",
    config = {},
  }
end

function M.rustfmt_formatter()
  return {
    "rustfmt",
    config = require("null-ls").builtins.formatting.rustfmt,
  }
end

local debug = require "user.lib.debug"
local program = function()
  return debug.find_executable(
    vim.fn.getcwd() .. "/" .. "target" .. "/" .. "debug"
  )
end
local processId = function()
  return debug.pgrep(
    debug.find_executable(vim.fn.getcwd() .. "/" .. "target" .. "/" .. "debug")
  )
end

-- function M.cppdbg_debugger()
--   return {
--     "cppdbg",
--     adapter = {
--       id = "cppdbg",
--       type = "executable",
--       command = "OpenDebugAD7",
--       options = (vim.fn.has "win32" == 1) and {
--         detached = false,
--       } or nil,
--     },
--     configs = {
--       rust = {
--         {
--           type = "cppdbg",
--           name = "Launch (cpptools)",
--           request = "launch",
--           MIMode = "gdb",
--           miDebuggerPath = debug.which "rust-gdb",
--           cwd = "${workspaceFolder}",
--           stopAtEntry = true,
--           program = program,
--           processId = processId,
--         },
--         {
--           type = "cppdbg",
--           name = "Attach (cpptools)",
--           request = "attach",
--           MIMode = "gdb",
--           miDebuggerPath = debug.which "rust-gdb",
--           cwd = "${workspaceFolder}",
--           program = program,
--           processId = processId,
--         },
--       },
--     },
--   }
-- end

function M.codelldb_debugger()
  return {
    "lldb",
    adapter = {
      type = "server",
      port = "${port}",
      executable = {
        command = "codelldb",
        args = { "--port", "${port}" },
        detached = vim.fn.has "win32" == 0,
      },
    },
    configs = {
      rust = {
        {
          type = "lldb",
          name = "Launch (codelldb)",
          request = "launch",
          cwd = "${workspaceFolder}",
          stopAtEntry = true,
          program = program,
          processId = processId,
        },
        {
          type = "lldb",
          name = "Attach (codelldb)",
          request = "attach",
          cwd = "${workspaceFolder}",
          program = program,
          processId = processId,
        },
      },
    },
  }
end

return M
