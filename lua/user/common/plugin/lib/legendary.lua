-- NOTE: define keymap, cmd, autocmd, and functions with a table and
-- NOTE: integrate with which-key

local M = { "mrjones2014/legendary.nvim" }

if require("user.lib.frontend").has_gui then
  M.requires = { "folke/which-key.nvim" }
  M.after = "which-key.nvim"
end

M.module = "legendary"

M.config = function()
  local legendary = require "legendary"

  legendary.setup {
    which_key = require("user.lib.frontend").has_gui and {
      auto_register = true,
    } or nil,
  }
end

return M
