-- NOTE: adds references to Luv documentation through Vim help

local M = { "nanotee/luv-vimdocs" }

return M
