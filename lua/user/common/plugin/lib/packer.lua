-- NOTE: package manager

local M = { "wbthomason/packer.nvim" }

M.requires = {
  -- NOTE: improves startup time by creating a lua bytecode cache
  { "lewis6991/impatient.nvim" },
}

return M
