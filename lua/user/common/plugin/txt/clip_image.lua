-- NOTE: copy image from clipboard, put into project and add
-- NOTE: into a Markdown file

local M = { "ekickx/clipboard-image.nvim" }

M.cmd = {
  "PasteImg",
}

-- TODO: wayland/xorg detection?
M.disable = not (
    (vim.fn.has "macunix" == 1 and vim.fn.executable "pngpaste" == 1)
    or (vim.fn.has "unix" == 1 and vim.fn.executable "xclip" == 1)
  )

M.config = function()
  local clip_image = require "clipboard-image"

  clip_image.setup {}
end

return M
