-- NOTE: add comments with gc and gcc

local M = { "numToStr/Comment.nvim" }

-- NOTE: enable the one from treesitter
M.config = require("user.lib.frontend").has_treesitter and nil
  or function()
    local comment = require "Comment"

    comment.setup {}
  end

return M
