-- NOTE: a bufferline

local M = { "akinsho/bufferline.nvim" }

M.disable = not require("user.lib.frontend").has_gui

M.tag = "*"

M.requires = {
  -- NOTE: close a lot of tabs at once
  { "kazhala/close-buffers.nvim" },

  -- NOTE: scopes buffers to tabs
  { "tiagovla/scope.nvim" },
}

M.config = function()
  local close = require "close_buffers"
  close.setup {
    preserve_window_layout = { "this" },
    next_buffer_cmd = function(windows)
      require("bufferline").cycle(1)
      local bufnr = vim.api.nvim_get_current_buf()

      for _, window in ipairs(windows) do
        vim.api.nvim_win_set_buf(window, bufnr)
      end
    end,
  }

  local scope = require "scope"
  scope.setup()

  local bufferline = require "bufferline"
  bufferline.setup {
    options = {
      numbers = "none",
      diagnostics = "nvim_lsp",
      diagnostics_indicator = function(count, level)
        local icon = level:match "error" and " "
          or level:match "warning" and ""
          or ""
        return " " .. icon .. count
      end,
      show_buffer_close_icons = false,
      offsets = {
        {
          filetype = "NvimTree",
          text = function()
            return string.format("%s", vim.fn.getcwd())
          end,
          highlight = "Directory",
          text_align = "center",
        },
      },
      sort_by = "extension",
      close_icon = "",
      custom_areas = {},
    },
  }
end

return M
