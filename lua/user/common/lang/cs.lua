local M = {}

function M.omnisharp_client()
  return {
    "omnisharp",
    config = {
      handlers = {
        ["textDocument/definition"] = require("omnisharp_extended").handler,
      },
    },
  }
end

function M.csharpier_formatter()
  return {
    "csharpier",
    config = require("null-ls").builtins.formatting.csharpier,
  }
end

function M.netcoredbg_debugger()
  return {
    "coreclr",
    adapter = {
      type = "executable",
      command = "netcoredbg",
      args = { "--interpreter=vscode" },
    },
    configs = {
      cs = {
        type = "netcoredbg",
        name = "attach - netcoredbg",
        request = "attach",
        processId = function()
          local pgrep_result = vim.fn.system "pgrep -f 'dotnet ./'"
          require("user.lib.log").info("Running netcoredbg on:", pgrep_result)
          return tonumber(pgrep_result)
        end,
      },
    },
  }
end

return M
