local M = {}

-- TODO: gui/not gui

function M.postload()
  local user = require "user.lib.user"
  local fn = require "user.lib.fn"
  user.keymaps {
    { "<leader>w", ":w" },
    { "<leader>W", ":SudaWrite" },
    { "gB", fn.pack_required("comment-box", "ccbox"), mode = "v" },
    { "gL", fn.pack_required("comment-box", "cline"), mode = "v" },
    { "r", fn.pack_required("substitute", "operator") },
    { "rr", fn.pack_required("substitute", "line") },
    { "R", fn.pack_required("substitute", "eol") },
    { "r", fn.pack_required("substitute", "visual"), mode = "x" },
    { "<leader>sr", "<Plug>SnipRun", mode = { "n", "v" } },
    { "<leader>fb", ":Telescope builtin<CR>" },
    { "<leader>ff", ":Telescope find_files<CR>" },
    { "<leader>fg", ":Telescope live_grep<CR>" },
    { "<leader>fc", ":Telescope commands<CR>" },
    { "<leader>fn", ":Telescope notify<CR>" },
    { "<leader>fm", ":Telescope media_files<CR>" },
    { "<leader>fe", ":Telescope emoji<CR>" },
    { "<leader>fi", ":IconPickerNormal emoji nerd_font alt_font symbols<CR>" },
    { "<leader>fh", ":Telescope help_tags<CR>" },
    { "<leader>fy", ":Telescope yank_history<CR>" },
    { "<leader>fp", ":Telescope projects<CR>" },
    { "<leader>fs", ":Telescope session<CR>" },
    {
      "<leader>fd",
      fn.pack(function()
        require("telescope").extensions.dap.configurations()
      end),
    },
    {
      "<leader>dt",
      fn.pack(function()
        require("dap").terminate()
      end),
    },
    { "<leader>tt", ":NvimTreeToggle<CR>" },
    { "gR", fn.pack_required("spectre", "open") },
    { "gR", fn.pack_required("spectre", "open_visual"), mode = { "v" } },
    { "<leader><leader>h", fn.pack_required("bufferline", "cycle", -1) },
    { "<leader><leader>l", fn.pack_required("bufferline", "cycle", 1) },
    { "<leader><leader>H", fn.pack_required("bufferline", "move", -1) },
    { "<leader><leader>L", fn.pack_required("bufferline", "move", 1) },
    {
      "<leader>co",
      fn.pack_required("close_buffers", "delete", { type = "other" }),
    },
    {
      "<leader>cs",
      "<cmd>Copilot panel<CR>",
    },
  }
end

function M.on_language_client_attach()
  local user = require "user.lib.user"
  local fn = require "user.lib.fn"
  user.keymaps {
    { "gh", "<cmd>Lspsaga lsp_finder<CR>" },
    { "gH", "<cmd>Trouble lsp_references<CR>" },
    { "<leader>ca", "<cmd>Lspsaga code_action<CR>", mode = { "n", "v" } },
    { "gd", "<cmd>Lspsaga peek_definition<CR>" },
    { "<leader>cd", "<cmd>Lspsaga show_cursor_diagnostics<CR>" },
    { "[e", "<cmd>Lspsaga diagnostic_jump_prev<CR>" },
    { "]e", "<cmd>Lspsaga diagnostic_jump_next<CR>" },
    { "gE", "<cmd>Trouble workspace_diagnostics<CR>" },
    {
      "[E",
      fn.pack_required(
        "lspsaga.diagnostic",
        "goto_prev",
        { severity = vim.diagnostic.severity.ERROR }
      ),
    },
    {
      "]E",
      fn.pack_required(
        "lspsaga.diagnostic",
        "goto_prev",
        { severity = vim.diagnostic.severity.ERROR }
      ),
    },
    { "<leader>o", "<cmd>Lspsaga outline<CR>" },
    { "K", "<cmd>Lspsaga hover_doc<CR>" },

    -- TODO: fix
    { mode = { "n", "t" }, "<A-d>", "<cmd>Lspsaga term_toggle fish<CR>" },
  }
end

return M
