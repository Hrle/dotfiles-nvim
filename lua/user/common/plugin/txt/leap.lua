-- NOTE: best for moving around - quite unique so hard to explain
-- NOTE: keymap is `s` or `S` and `f/F` and `t/T` are enhanced

local M = { "ggandor/leap.nvim" }

M.requires = {
  {
    -- NOTE: adds smart f/F/t/T to leap.nvim
    "ggandor/flit.nvim",
    config = function()
      local flit = require "flit"

      flit.setup {}
    end,
  },
}

function M.config()
  local leap = require "leap"
  local frontend = require "user.lib.frontend"

  leap.setup {}
  leap.add_default_mappings()

  -- NOTE: greys out the search area
  if frontend.has_gui then
    vim.api.nvim_set_hl(0, "LeapBackdrop", { link = "Comment" })
  end
end

return M
