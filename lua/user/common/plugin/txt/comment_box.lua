-- NOTE: create comment boxes around text

local M = { "LudoPinelli/comment-box.nvim" }

M.module = "comment-box"

M.config = function()
  local comment_box = require "comment-box"
  comment_box.setup {
    doc_width = 80,
    box_width = 60,
    line_width = 70,
  }
end

return M
