-- NOTE: adds references to Lua documentation through Vim help

local M = { "milisims/nvim-luaref" }

return M
