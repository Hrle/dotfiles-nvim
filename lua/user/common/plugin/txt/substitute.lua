-- NOTE: replaces selection or line with register

local M = { "gbprod/substitute.nvim" }

-- TODO: with yanky hydra

M.module = "substitute"

M.config = function()
  local substitute = require "substitute"

  local has_yanky, _ = pcall(require, "yanky")
  local on_substitute = nil
  if has_yanky then
    on_substitute = function(event)
      local yanky = require "yanky"
      yanky.init_ring(
        "p",
        event.register,
        event.count,
        event.vmode:match "[vV]"
      )
    end
  end

  substitute.setup {
    on_substitute = on_substitute,
  }
end

return M
