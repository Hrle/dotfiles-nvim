local M = {}

-- NOTE: with undersocre because tree plugins ignore vscode stuff
M.vscode = "vs_code"
M.terminal = "terminal"
M.disabled = os.getenv "NVIM_DOTFILE_FRONTEND_DISABLED" and true or false

if vim.g.vscode then
  M.name = M.vscode
  M.has_gui = false
  M.has_treesitter = false
  M.has_lsp = false
  M.has_debug = false
  M.has_session = false
else
  M.name = M.terminal
  M.has_gui = true
  M.has_treesitter = true
  M.has_lsp = true
  M.has_debug = true
  M.has_session = true
end

return M
