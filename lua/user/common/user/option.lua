local M = {}

local function options()
  local fs = require "user.lib.fs"
  local path = require "user.lib.path"

  -- this is in the order of the options with ':opt' command

  -- 1 important

  -- don't be compatible with vi
  vim.opt.compatible = false

  -- 2 moving around, searching, patterns

  -- case insensitive
  vim.opt.ignorecase = true

  -- case sensitive when a pattern contains upper chars
  vim.opt.smartcase = true

  -- incremental search highlight
  vim.opt.incsearch = true

  -- 3 tags

  -- 4 text display

  -- set maximum scrolling offset so the cursor is always
  -- in the center of the screen
  vim.opt.scrolloff = 999

  -- 5 syntax, spelling and highlight

  -- highlight search
  vim.opt.hlsearch = true

  -- disable Vim spellchecking
  vim.opt.spell = false

  -- treat camel-cased words as separate
  vim.opt.spelloptions = "camel"

  -- 6 multiple windows

  -- 7 multiple tab pages

  -- 8 terminal

  -- 9 using the mouse

  -- 10 printing

  -- 11 messages and info

  -- messages when file is written and stuff like that
  -- TODO: fix
  vim.opt.shortmess:append { s = true, W = true, I = true, c = true }

  -- 12 selecting text

  -- TODO: fix
  -- use system clipboard
  vim.opt.clipboard = "unnamedplus"

  -- 13 editing text

  -- wrap above 80 chars
  vim.opt.textwidth = 80

  -- wrap at 80 chars
  vim.opt.wrapmargin = 80

  -- read ":h fo-table"
  vim.opt.formatoptions = "t,c,r,o,q,n,2,m,M,1,j,p"

  -- use ~ like an operator to change casing
  vim.opt.tildeop = true

  -- join lines with '.' without adding two spaces?? this one is so weird
  vim.opt.joinspaces = false

  -- 14 tabs and indenting

  -- number of columns occupied by a tab
  vim.opt.tabstop = 2

  -- width for auto-indents
  vim.opt.shiftwidth = 2

  -- use 2 spaces when <BS> on tabs
  vim.opt.softtabstop = 2

  -- use 'shiftwidth' for '<<' and '>>'
  vim.opt.shiftround = true

  -- converts tabs to white space
  vim.opt.expandtab = true

  -- indent a new line the same amount as the line just typed
  vim.opt.autoindent = true

  -- 15 folding

  -- TODO: config
  -- 16 diff mode

  -- 17 mapping

  -- i would set noremap here, but that would probably break plugins

  -- 18 reading and writing files

  -- last line doesn't have end of line - this just sounds confusing when the
  -- character is not present
  vim.opt.endofline = false

  -- i don't know why people add end-of-line at the end of a file
  vim.opt.fixendofline = false

  -- Byte Order Mark at the beginning of a file is a Microsoft thing i think
  vim.opt.bomb = false

  -- keep a backup file before overwriting a file
  vim.opt.backup = true

  -- backup file directory so nvim doesn't create backup files all over
  -- the place
  -- double slash is to add the full path of files as name of file with some
  -- magic
  vim.opt.backupdir = path.data_backup_dir .. "//"
  fs.ensure_dir(path.data_backup_dir)

  -- 19 the swap file

  -- use swap files
  vim.opt.swapfile = true

  -- swap file directory - double slash is to add the full path of files
  -- as name
  -- of file so nvim can find that stuff
  vim.opt.directory = path.data_swap_dir .. "//"
  fs.ensure_dir(path.data_swap_dir)

  -- num of chars to update the swap file
  vim.opt.updatecount = 100

  -- time to update the swap file and CursorHold events
  vim.opt.updatetime = 300

  -- 20 command line editing

  -- 21 executing external commands

  -- 22 running make and jumping to errors

  -- 23 language specific

  -- 24 multi-byte characters

  -- 25 various

  -- load plugin scripts on startup - needed for any plugin manager
  vim.opt.loadplugins = true

  -- use g by default in substitutions
  vim.opt.gdefault = true

  -- what to save with view files
  vim.opt.viewoptions = "folds,options,cursor,curdir"

  -- view file directory
  -- double slash is to add the full path of files as name of file so
  -- nvim can find that stuff
  vim.opt.viewdir = path.data_view_dir .. "//"
  fs.ensure_dir(path.data_view_dir)

  -- session
  vim.opt.sessionoptions =
    "blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal"

  -- python 2 is deprecated
  vim.opt.pyxversion = 3

  -- other - not from `:opt`

  -- NOTE: don't put this in mapping
  vim.g.mapleader = " "
end

local function gui_options()
  local fs = require "user.lib.fs"
  local path = require "user.lib.path"

  -- Neovide
  vim.g.neovide_input_use_logo = true
  vim.g.neovide_cursor_vfx_mode = "ripple"

  -- this is in the order of the options with ':opt' command

  -- 1 important

  -- 2 moving around, searching, patterns

  -- 3 tags

  -- 4 text display

  -- add line numbers
  vim.opt.number = false

  -- numbers relative to cursor
  vim.opt.relativenumber = false

  -- number column width
  vim.opt.numberwidth = 4

  -- preserve indentation in wrapped text
  vim.opt.breakindent = true

  -- make break indentation at least 2 chars, and shift breaks by 2 chars put
  -- 'showbreak' at the beginning of break indented lines
  vim.opt.breakindentopt = "min:2,shift:2,sbr"

  -- show this at the beginning of break indented lines
  vim.opt.showbreak = "↳"

  -- extra info
  vim.opt.list = true
  vim.opt.listchars = { trail = "·", tab = "⇥ " }
  vim.opt.fillchars = { fold = " ", vert = " ", eob = " " }

  -- don't redraw when executing macros
  vim.opt.lazyredraw = false

  -- 5 syntax, spelling and highlight

  -- syntax highlighting
  vim.cmd [[ syntax on ]]

  -- allow auto-indenting and plugins depending on file type
  vim.cmd [[ filetype plugin indent on ]]

  -- use true colors in terminal
  -- required for material theme plugin
  vim.opt.termguicolors = true

  -- highlight the cursor column
  vim.opt.cursorcolumn = false

  -- highlight cursor line
  vim.opt.cursorline = false

  -- set an 81 column border for good coding style
  -- NOTE: must be a string
  vim.opt.colorcolumn = "82"

  -- 6 multiple windows

  -- display one global statusline
  vim.opt.laststatus = 3

  -- on vsplit or vnew open a new window to the left of the current one
  vim.opt.splitright = false

  -- on split or new open a new window below the current one
  vim.opt.splitbelow = true

  -- 7 multiple tab pages

  -- 8 terminal

  -- 9 using the mouse

  -- 10 printing

  -- 11 messages and info

  -- 12 selecting text

  -- 13 editing text

  -- use undo files
  vim.opt.undofile = true

  -- undo file directory so nvim doesn't create undo files all over the place
  -- always starts in your $HOME directory
  -- double slash is to add the full path of files as name of file so
  -- nvim can find that stuff
  vim.opt.undodir = path.data_undo_dir .. "//"
  fs.ensure_dir(path.data_undo_dir)

  -- read ":h 'complete'"
  vim.opt.complete = ".,w,b,u,k,s,t"

  -- list completions in insert mode
  vim.opt.completeopt = "menu,menuone,noselect"

  -- 14 tabs and indenting

  -- 15 folding

  -- starting fold level
  vim.opt.foldlevel = 0

  -- maximum fold level
  vim.opt.foldnestmax = 3

  -- TODO: config
  -- 16 diff mode

  -- 17 mapping

  -- i would set noremap here, but that would probably break plugins

  -- timeout for popups to appear
  vim.opt.timeoutlen = 400

  -- 18 reading and writing files

  -- 19 the swap file

  -- 20 command line editing

  -- don't list completions in command mode because I'm using
  -- 'gelguy/wilder.nvim'
  vim.opt.wildmenu = false

  -- command mode complation character
  -- NOTE: https://github.com/neovim/neovim/issues/11042#issuecomment-922644043
  -- NOTE: lua API says it is a number option, so it is easier to configure
  -- through Vimscript
  vim.cmd [[ :set wildchar=<C-N> ]]

  -- see ':h 'wildmode''
  -- keeping it here in case I need it
  vim.opt.wildmode = "longest,list,full"

  -- ignore case when completing file names
  -- keeping it here in case I need it
  vim.opt.wildignorecase = true

  -- 21 executing external commands

  -- warn when using shell command when the buffer has changes
  vim.opt.warn = true

  -- 22 running make and jumping to errors

  -- 23 language specific

  -- 24 multi-byte characters

  -- use utf-8
  vim.opt.encoding = "utf-8"

  -- use utf-8 (buffer-local)
  vim.opt.fileencoding = "utf-8"

  -- emojis as full width
  vim.opt.emoji = true

  -- 25 various

  -- always display the column on the left
  vim.opt.signcolumn = "yes:1"

  -- other - not from `:opt`

  -- font
  vim.opt.guifont = "JetBrainsMono Nerd Font,DejaVu Sans,Noto Sans:h16"

  -- speed up scrolling
  vim.opt.ttyfast = true

  -- set quickfix text
  vim.opt.qftf = '{info -> v:lua.require("user.lib.quickfix").qftf(info)}'

  -- LSP logging
  vim.lsp.set_log_level "info"
  if vim.fn.has "nvim-0.5.1" == 1 then
    ---@diagnostic disable-next-line: param-type-mismatch
    require("vim.lsp.log").set_format_func(vim.inspect)
  end
end

function M.preload()
  local frontend = require "user.lib.frontend"

  options()
  if frontend.has_gui then
    gui_options()
  end
end

function M.postload()
  -- TODO: doesn't work well with lspsaga windows
  -- if require("user.lib.frontend").has_gui then
  --   vim.opt.foldmethod = "expr"
  --   vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
  -- end
end

return M
