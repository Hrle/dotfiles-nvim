local M = {}

function M.stack(name)
  local const = require "user.lib.const"
  local result = {}

  name = name or "Stack"
  table.insert(result, name)
  table.insert(result, ":")
  table.insert(result, const.newline)
  local index = 1
  local current = debug.getinfo(index)
  table.insert(result, const.newline)

  repeat
    table.insert(result, index)
    if current.short_src == "[C]" then
      table.insert(result, "<compiled>")
      if not current.name then
        table.insert(result, "<capture>")
      else
        table.insert(result, current.name)
      end
    elseif not current.name then
      table.insert(result, "<capture>")
      table.insert(result, current.currentline)
      table.insert(result, current.short_src)
    else
      table.insert(result, current.name)
      table.insert(result, current.currentline)
      table.insert(result, current.short_src)
    end
    table.insert(result, const.newline)
    index = index + 1
    current = debug.getinfo(index)
  until current == nil
  table.insert(result, const.newline)

  return result
end

function M.print_stack(name)
  print(unpack(M.stack(name)))
end

function M.try(unsafe, message, ...)
  local did_run, result = pcall(unsafe, ...)
  if not did_run then
    local log = require "user.lib.log"
    log.error(message, ":", result)
    return
  end
  return result
end

function M.pack(unsafe, ...)
  local capture = { ... }
  return function(...)
    return require("user.lib.fn").try(
      unsafe,
      string.format("Failed invoking %s", unsafe),
      unpack(capture),
      ...
    )
  end
end

function M.pack_required(module, name, ...)
  local capture = { ... }
  return function(...)
    return require("user.lib.fn").try(function(...)
      local unsafe = require(module)[name]
      return unsafe(unpack(capture), ...)
    end, string.format("Failed invoking %s from %s", name, module), ...)
  end
end

-- NOTE: this function is **evil**
-- function M.capture(state, call)
--   return setmetatable(state or {}, {
--     __call = call,
--   })
-- end

return M
