local M = {}

-- TODO: guid/not gui

function M.postload()
  local user = require "user.lib.user"
  local fn = require "user.lib.fn"

  user.commands {
    {
      ":ReloadConfig",
      fn.pack(function()
        local _user = require "user.lib.user"
        local log = require "user.lib.log"

        log.info "Reloading config"

        _user.preload()
        _user.postload()
      end),
      description = "Reload config",
    },
    {
      ":SyncPlugins",
      fn.pack(function()
        local pack = require "user.lib.pack"

        pack.sync()
      end),
      description = "Sync plugins",
    },
    {
      ":ReloadPlugins",
      fn.pack(function()
        local pack = require "user.lib.pack"
        local log = require "user.lib.log"

        log.info "Reloading plugins"

        pack.compile()
      end),
      description = "Reload plugins",
    },
    {
      ":ReloadModule",
      fn.pack(function()
        local module = require "user.lib.module"
        local log = require "user.lib.log"

        local path = module.current()
        local name = module.name(path)
        log.info("Reloading module", name, "at", path)

        module.unload(name)
        require(name)
      end),
      description = "Reload buffer module",
    },
    {
      ":DebugConfig",
      fn.pack(function()
        local log = require "user.lib.log"

        log.level = "debug"
      end),
      description = "Set config log level to debug",
    },
    {
      ":ExitConfigDebug",
      fn.pack(function()
        local log = require "user.lib.log"

        log.level = "warn"
      end),
      description = "Return config log level to warn",
    },
    {
      ":PasteImg",
      description = "Paste image from clipboard into project and add reference on cursor position",
    },
  }
end

return M
