-- NOTE: add surrounding pairs around text objects

local M = { "kylechui/nvim-surround" }

M.config = function()
  local surround = require "nvim-surround"

  surround.setup {}
end

return M
