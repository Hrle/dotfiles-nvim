local M = {}

function M.current()
  local root = require("user.lib.path").conf_root_dir .. "/lua/"

  local path = vim.api.nvim_buf_get_name(0)
  -- NOTE: Neovim returns paths with a protocol in the beginning
  local fixed = string.gsub(path, string.format(".*%s", root), root, 1)

  return fixed
end

function M.path(name)
  local root = require("user.lib.path").conf_root_dir .. "/lua/"
  local path = root .. string.gsub(name, ".", "/") .. ".lua"
  return path
end

function M.name(path)
  local root = require("user.lib.path").conf_root_dir .. "/lua/"

  -- NOTE: Neovim returns paths with a protocol in the beginning
  local relative = string.gsub(path, string.format(".*%s", root), "", 1)
  local extensionless = vim.fn.fnamemodify(relative, ":r")
  local name = string.gsub(extensionless, "/", ".")

  return name
end

function M.tail(path)
  return vim.fn.fnamemodify(path, ":t:r")
end

function M.mirror_vim_module_path(path)
  return string.gsub(path, "lua", "vim")
end

function M.unload(name)
  package.loaded[name] = nil

  return true
end

function M.with(path, todo)
  local name = M.name(path)
  require("user.lib.fn").try(function()
    local module = dofile(path)
    return todo(module, path, name)
  end, string.format("Failed loading module %s at %s", name, path))
end

function M.foreach(path, todo)
  local module = require "user.lib.module"

  local glob = vim.fn.glob(path)
  for match in string.gmatch(glob, "%S+") do
    module.with(match, todo)
  end
end

function M.run(path, pattern, ...)
  local module = require "user.lib.module"

  local capture = { ... }
  module.foreach(path, function(local_module)
    if type(local_module) == "table" then
      for key, value in pairs(local_module) do
        if
          vim.regex("\\v" .. pattern):match_str(key)
          and type(value) == "function"
        then
          value(unpack(capture))
        end
      end
    end
  end)
end

function M.run_and(path, pattern, todo, ...)
  local module = require "user.lib.module"

  local capture = { ... }
  module.foreach(path, function(local_module, local_path, local_name)
    if type(local_module) == "table" then
      for key, value in pairs(local_module) do
        if
          vim.regex("\\v" .. pattern):match_str(key)
          and type(value) == "function"
        then
          local result = value(unpack(capture))
          todo(result, local_path, local_name)
        end
      end
    end
  end)
end

function M.source(path)
  return require("user.lib.fn").try(
    vim.cmd,
    string.format("Failed loading vim module at %s", path),
    string.format("source %s", path)
  )
end

function M.source_glob(path)
  for match in string.gmatch(vim.fn.glob(path), "%S+") do
    M.source(match)
  end
end

return M
