-- NOTE: completion engine for Neovim
local M = { "ms-jpq/coq_nvim" }

-- NOTE: disabled because it doesn't play well with noice
-- M.disable = not require("user.lib.frontend").has_gui
M.disable = true

M.branch = "coq"

M.requires = {
  -- NOTE: snippets
  { "ms-jpq/coq.artifacts", branch = "artifacts" },

  -- NOTE: third party completion sources
  { "ms-jpq/coq.thirdparty", branch = "3p" },
}

M.setup = function()
  vim.g.coq_settings = {
    auto_start = true,
    xdg = vim.fn.has "unix" == 1 and true or false,
    keymap = {
      pre_select = true,
    },
  }
end

return M
