-- NOTE: colorscheme
local M = { "xiyaowong/nvim-transparent" }

M.disable = not require("user.lib.frontend").has_gui

M.requires = {
  { "marko-cerovac/material.nvim" },
  { "sainnhe/edge" },
  { "sainnhe/sonokai" },
  { "sainnhe/everforest" },
  { "navarasu/onedark.nvim" },
}

M.themes = {
  "sonokai",
  "edge",
  "onedark",
  "everforest",
  "material",
}

M.config = function()
  local transparent = require "transparent"
  transparent.setup {
    enable = false,
    groups = {
      "Normal",
      "Comment",
      "Constant",
      "Special",
      "Identifier",
      "Statement",
      "PreProc",
      "Type",
      "String",
      "Function",
      "Conditional",
      "Repeat",
      "Operator",
      "Structure",
      "LineNr",
      "NonText",
      "CursorLineNr",
      "EndOfBuffer",
      "SignColumn",
      "EndOfBuffer",
      "BufferLineTabClose",
      "BufferlineBufferSelected",
      "BufferLineFill",
      "BufferLineBackground",
      "BufferLineSeparator",
      "BufferLineIndicatorSelected",
      "NvimTreeNormal",
      "Pmenu",
      "PmenuSbar",
    },
  }

  local has_material, material = pcall(require, "material")
  vim.g.material_style = "palenight"
  if has_material then
    material.setup {
      high_visibility = { darker = true },
      disable = {
        eob_lines = true,
        background = true,
        borders = true,
      },
    }
  end

  vim.g.sonokai_style = "andromeda"
  vim.g.sonokai_better_performance = 1
  vim.g.sonokai_transparent_background = 2
  vim.g.sonokai_enable_italic = 1
  vim.g.sonokai_show_eob = 0

  vim.g.edge_style = "neon"
  vim.g.edge_dim_foreground = 1
  vim.g.edge_better_performance = 1
  vim.g.edge_transparent_background = 2
  vim.g.edge_enable_italic = 1
  vim.g.edge_show_eob = 0
  vim.g.edge_diagnostic_virtual_text = "colored"

  vim.g.everforest_background = "hard"
  vim.g.everforest_ui_contrast = "high"
  vim.g.everforest_better_performance = 1
  vim.g.everforest_transparent_background = 2
  vim.g.everforest_enable_italic = 1
  vim.g.everforest_show_eob = 0
  vim.cmd [[ set background=dark ]]

  local has_onedark, onedark = pcall(require, "onedark")
  if has_onedark then
    onedark.setup {
      style = "darker",
      transparent = true,
      lualine = {
        transparent = true,
      },
    }
  end

  vim.cmd [[ colorscheme edge ]]
end

return M
