-- NOTE: cmdline UI
local M = { "folke/noice.nvim" }

-- TODO: PR for positioning notify
-- TODO: fix colorcolumn

local frontend = require "user.lib.frontend"
M.disable = not (frontend.has_gui and frontend.has_lsp)

M.requires = {
  -- NOTE: UI library
  { "MunifTanjim/nui.nvim" },

  -- NOTE: search and replace
  { "windwp/nvim-spectre" },

  -- NOTE: peeks jump location when using `:{number}`
  { "nacro90/numb.nvim" },

  -- NOTE: Notify
  { "rcarriga/nvim-notify" },

  -- NOTE: UI library
  { "stevearc/dressing.nvim" },

  -- NOTE: search highlight as virtual text
  { "kevinhwang91/nvim-hlslens", tag = "*" },

  -- NOTE: column
  { "lukas-reineke/virt-column.nvim", tag = "*" },

  -- NOTE: indent highlight
  {
    "lukas-reineke/indent-blankline.nvim",
    disable = true,
  },

  -- NOTE: folds
  { "anuvyklack/pretty-fold.nvim" },

  -- NOTE: fold preview
  {
    "anuvyklack/fold-preview.nvim",
    requires = {
      { "anuvyklack/keymap-amend.nvim" },
    },
  },

  -- NOTE: incremental rename
  { "smjonas/inc-rename.nvim" },

  -- NOTE: colorizes hex colors
  { "norcalli/nvim-colorizer.lua" },

  -- NOTE: color picker - use it when theres a filetype/buftype on the buffer
  {
    "nvim-colortils/colortils.nvim",
    tag = "*",
    disable = true,
  },

  -- NOTE: icon picker
  {
    "ziontee113/icon-picker.nvim",
    requires = {
      { "stevearc/dressing.nvim" },
    },
  },
}

M.config = function()
  local highlight = require "hlslens"
  highlight.setup {
    calm_down = true,
    nearest_only = true,
  }

  local numb = require "numb"
  numb.setup {}

  local column = require "virt-column"
  column.setup {}

  local spectre = require "spectre"
  spectre.setup {}

  local has_indent, indent = pcall(require, "indent_blankline")
  if has_indent then
    indent.setup {
      use_treesitter = require("user.lib.frontend").has_treesitter,
      use_treesitter_scope = require("user.lib.frontend").has_treesitter,
      show_current_context = true,
      show_current_context_start = true,
      buftype_exclude = { "help", "terminal" },
      filetype_exclude = {
        "dashboard",
        "startup",
        "packer",
        "NvimTree",
        "Telescope",
        "noice",
        "notify",
      },
    }
  end

  local colors = require "colorizer"
  colors.setup({
    "*",
  }, {
    RGB = true,
    RRGGBB = true,
    RRGGBBAA = true,
    rgb_fn = true,
    hsl_fn = true,
    names = false,
  })

  local has_color_picker, color_picker = pcall(require, "colortils")
  if has_color_picker then
    color_picker.setup {}
  end

  local has_icon_picker, icon_picker = pcall(require, "icon-picker")
  if has_icon_picker then
    icon_picker.setup {
      disable_legacy_commands = true,
    }
  end

  local has_pretty_fold, pretty_fold = pcall(require, "pretty-fold")
  if has_pretty_fold then
    pretty_fold.setup {
      ft_ignore = { "lspsagaoutline", "neorg" },
    }
  end

  local fold_preview = require "fold-preview"
  fold_preview.setup {}

  local has_inc_rename, inc_rename = pcall(require, "inc_rename")
  if has_inc_rename then
    inc_rename.setup {}
    vim.keymap.set("n", "gr", function()
      return ":IncRename " .. vim.fn.expand "<cword>"
    end, { expr = true })
  end

  local has_dressing, dressing = pcall(require, "dressing")
  if has_dressing then
    dressing.setup {
      input = {
        win_options = {
          winblend = 0,
          winhighlight = "Normal:Normal,FloatBorder:Float",
        },
      },
      select = {
        backend = { "fzf_lua", "telescope", "builtin" },
      },
    }
  end

  local noice = require "noice"
  noice.setup {
    routes = {
      {
        filter = {
          event = "msg_show",
          kind = "",
          find = "written",
        },
        opts = { skip = true },
      },
    },
    presets = {
      bottom_search = true,
      command_palette = true,
      inc_rename = true,
      lsp_doc_border = true,
    },
    views = {
      hover = {
        win_options = {
          winblend = 0,
        },
      },
      mini = {
        border = {
          style = "rounded",
        },
        position = {
          row = -2,
        },
        win_options = {
          winblend = 0,
          winhighlight = {
            Normal = "Normal",
            FloatBorder = "Float",
          },
        },
      },
      popup = {
        win_options = {
          winhighlight = {
            Normal = "Normal",
            FloatBorder = "Float",
          },
        },
      },
    },
    lsp = {
      progress = {
        format = {
          { " {data.progress.percentage}% " },
          { "{spinner} ", hl_group = "NoiceLspProgressSpinner" },
          { "{data.progress.title} ", hl_group = "Normal" },
          { "{data.progress.client} ", hl_group = "NoiceLspProgressClient" },
        },
        format_done = {
          { " ✔ ", hl_group = "NoiceLspProgressSpinner" },
          { "{data.progress.title} ", hl_group = "Normal" },
          { "{data.progress.client} ", hl_group = "NoiceLspProgressClient" },
        },
      },
      override = {
        ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
        ["vim.lsp.util.stylize_markdown"] = true,
        ["cmp.entry.get_documentation"] = true,
      },
    },
  }

  local notify = require "notify"
  notify.setup {
    background_colour = "#000000",
  }
  vim.notify = notify
end

return M
