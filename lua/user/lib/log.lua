local M = {}

local levels = {
  trace = 0,
  debug = 1,
  info = 2,
  warn = 3,
  error = 4,
}

local function level_to_num(level)
  return levels[level] or levels.warn
end

local function get_level()
  return M.level or "warn"
end

local function should_log(level)
  return level_to_num(level) >= level_to_num(get_level())
end

local function log(level, ...)
  if not should_log(level) then
    return
  end

  M.logger(level, M.formatter(...))
end

function M.is_valid(level)
  return levels[level] ~= nil
end

function M.concat(...)
  local result = {}
  for _, value in ipairs { ... } do
    if type(value) == "table" then
      table.insert(result, vim.inspect(value))
    elseif type(value) == "function" then
      table.insert(result, "<function>")
    elseif type(value) == "thread" then
      table.insert(result, "<thread>")
    elseif type(value) == "userdata" then
      table.insert(result, "<userdata>")
    elseif type(value) == "boolean" then
      if value then
        table.insert(result, "true")
      else
        table.insert(result, "false")
      end
    else
      table.insert(result, value)
    end
  end
  return table.concat(result, " ")
end

function M.console(level, message)
  print(message)

  if level == "error" then
    require("user.lib.fn").print_stack()
  end
end

M.formatter = M.concat

M.logger = M.console

function M.trace(...)
  log("trace", ...)
end

function M.debug(...)
  log("debug", ...)
end

function M.info(...)
  log("info", ...)
end

function M.warn(...)
  log("warn", ...)
end

function M.error(...)
  log("error", ...)
end

M.level = os.getenv "NVIM_DOTFILE_LOG_LEVEL"
if M.level then
  if levels[M.level] then
    M.info("Logging level set to", M.level)
  else
    M.warn("Logging level set to", M.level, "which is not supported")
  end
else
  M.level = "warn"
end

-- NOTE: this instead of capture so you can require this module
-- NOTE: from user.lib.fn
return setmetatable(M, {
  __call = function(this, level, ...)
    this.info(level, ...)
  end,
})
