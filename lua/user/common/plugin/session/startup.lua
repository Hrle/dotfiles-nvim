-- NOTE: displays a nice dashboard when you start Neovim
local M = { "startup-nvim/startup.nvim" }

-- TODO: fix when available

M.requires = {
  -- NOTE: library
  { "nvim-lua/plenary.nvim" },

  -- NOTE: menu
  { "nvim-telescope/telescope.nvim" },

  -- NOTE: UI library
  { "MunifTanjim/nui.nvim" },

  -- NOTE: execute project local config
  { "MunifTanjim/exrc.nvim" },

  -- NOTE: automatically save/restore session
  { "rmagatti/auto-session" },
}

local frontend = require "user.lib.frontend"
M.disable = not (frontend.has_session and frontend.has_gui)

M.config = function()
  local startup = require "startup"
  startup.setup {
    header = {
      type = "text",
      align = "center",
      fold_section = false,
      title = "Header",
      margin = 5,
      content = require("startup.headers").hydra_header,
      highlight = "Statement",
      default_color = "",
      oldfiles_amount = 0,
    },
    quote = {
      type = "text",
      oldfiles_directory = false,
      align = "center",
      fold_section = false,
      title = "Quote",
      margin = 5,
      content = require("startup.functions").quote(),
      highlight = "Constant",
      default_color = "",
      oldfiles_amount = 0,
    },
    options = {
      after = function()
        require("startup.utils").oldfiles_mappings()
      end,
      mapping_keys = true,
      cursor_column = 0.5,
      empty_lines_between_mappings = true,
      disable_statuslines = true,
      paddings = { 2, 2, 2, 2, 2, 2, 2 },
    },
    colors = {
      background = "#1f2227",
      folded_section = "#56b6c2",
    },
    parts = {
      "header",
      "quote",
    },
  }

  local exrc = require "exrc"
  exrc.setup {
    files = {
      ".nvimrc.lua",
      ".nvimrc",
      ".exrc.lua",
      ".exrc",
    }
  }

  local path = require "user.lib.path"
  local fs = require "user.lib.fs"
  local auto_session = require "auto-session"
  local pre_restore_cmds = {}
  local has_tree, _ = pcall(require, "nvim-tree.lib")
  if has_tree then
    table.insert(pre_restore_cmds, function()
      local tree = require "nvim-tree.lib"
      tree.change_dir(vim.fn.getcwd())
      tree.refresh_tree()
    end)
  end
  fs.ensure_dir(path.data_session_dir)
  auto_session.setup {
    auto_session_root_dir = path.data_session_dir,
    auto_session_enabled = true,
    auto_save_enabled = true,
    auto_restore_enabled = true,
    auto_session_enable_last_session = true,
    pre_restore_cmds = pre_restore_cmds,
  }

  local user = require "user.lib.user"
  user.autocmds {
    {
      name = "UserStartupAutocmds",
      {
        "Filetype",
        function()
          vim.wo.colorcolumn = ""
          vim.cmd [[ file Startup ]]
        end,
        opts = {
          pattern = "startup",
        },
      },
      {
        "BufDelete",
        function()
          vim.wo.colorcolumn = "81"
        end,
        opts = {
          pattern = "Startup",
        },
      },
    },
  }
end

return M
