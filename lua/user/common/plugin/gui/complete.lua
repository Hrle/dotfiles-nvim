-- NOTE: completion
local M = { "hrsh7th/nvim-cmp" }

M.tag = "*"

M.disable = not require("user.lib.frontend").has_gui

M.requires = {
  -- NOTE: buffer completion source
  { "hrsh7th/cmp-buffer" },

  -- NOTE: path completion source
  { "hrsh7th/cmp-path" },

  -- NOTE: Neovim Lua completion source
  { "hrsh7th/cmp-nvim-lua" },

  -- NOTE: LSP completion source
  { "hrsh7th/cmp-nvim-lsp" },

  -- NOTE: Neovim snippets
  { "L3MON4D3/LuaSnip", tag = "*" },

  -- NOTE: snippets imported from VS code
  { "rafamadriz/friendly-snippets" },

  -- NOTE: LuaSnip completion source
  { "saadparwaiz1/cmp_luasnip" },

  -- NOTE: Tabnine completion source
  {
    "tzachar/cmp-tabnine",
    run = vim.fn.has "win32" == 1 and "pwsh install.ps1" or "bash install.sh",
  },

  -- NOTE: NPM completion source
  { "David-Kunz/cmp-npm", requires = "nvim-lua/plenary.nvim" },

  -- NOTE: icons for completion sources
  {
    "onsails/lspkind-nvim",
    config = function()
      local lspkind = require "lspkind"
      lspkind.init {}
    end,
  },

  { "github/copilot.vim" },
}

M.after = "nvim-autopairs"

M.config = function()
  local cmp = require "cmp"

  local source_mapping = {
    buffer = "[Buf]",
    path = "[Path]",
    nvim_lua = "[Nvim]",
    nvim_lsp = "[LSP]",
    luasnip = "[Snip]",
    cmp_tabnine = "[TN]",
    npm = "[NPM]",
  }
  local function format(entry, vim_item)
    local lspkind = require "lspkind"
    vim_item.kind = lspkind.presets.default[vim_item.kind]
    local menu = source_mapping[entry.source.name]
    if entry.source.name == "cmp_tabnine" then
      if
        entry.completion_item.data ~= nil
        and entry.completion_item.data.detail ~= nil
      then
        menu = entry.completion_item.data.detail .. " " .. menu
      end
      vim_item.kind = ""
    end
    vim_item.menu = menu
    return vim_item
  end

  local function snip_jump_next(fallback)
    local luasnip = require "luasnip"
    local _cmp = require "cmp"
    if luasnip.expand_or_jumpable() then
      luasnip.expand_or_jump()
    elseif _cmp.visible() then
      _cmp.select_next_item()
    else
      fallback()
    end
  end

  local function snip_jump_prev(fallback)
    local luasnip = require "luasnip"
    local _cmp = require "cmp"
    if luasnip.jumpable(-1) then
      luasnip.jump(-1)
    elseif _cmp.visible() then
      _cmp.select_prev_item()
    else
      fallback()
    end
  end

  cmp.setup {
    sources = cmp.config.sources {
      { name = "npm" },
      { name = "cmp_tabnine" },
      { name = "luasnip" },
      { name = "nvim_lua" },
      { name = "nvim_lsp" },
      { name = "buffer" },
      { name = "path" },
    },
    snippet = {
      expand = function(args)
        local luasnip = require "luasnip"
        luasnip.lsp_expand(args.body)
      end,
    },
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert {
      ["<C-Space>"] = cmp.mapping(cmp.mapping.complete {}, { "i", "c" }),
      ["<CR>"] = cmp.mapping.confirm {
        behavior = cmp.ConfirmBehavior.Replace,
        select = true,
      },
      ["<A-n>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),
      ["<A-p>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),
      ["<A-c>"] = cmp.mapping {
        i = cmp.mapping.abort(),
        c = cmp.mapping.close(),
      },
      ["<Tab>"] = cmp.mapping(snip_jump_next, { "i", "c" }),
      ["<S-Tab>"] = cmp.mapping(snip_jump_prev, { "i", "c" }),
    },
    formatting = { format = format },
    experimental = {
      ghost_text = true,
    },
  }

  local autopairs_cmp = require "nvim-autopairs.completion.cmp"
  cmp.event:on("confirm_done", autopairs_cmp.on_confirm_done())

  local module = require "user.lib.module"
  local path = require "user.lib.path"
  module.run(path.conf_lua_common_lang_pat, "snippet")
  module.run(path.conf_lua_frontend_lang_pat, "snippet")

  vim.cmd [[
    imap <silent><script><expr> <C-J> copilot#Accept("\<CR>")
    let g:copilot_no_tab_map = v:true
  ]]
end

return M
